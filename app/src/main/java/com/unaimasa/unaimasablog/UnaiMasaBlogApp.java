package com.unaimasa.unaimasablog;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.MobileAds;
import com.unaimasa.unaimasablog.util.media.ImageLoaderConfigurator;

import io.fabric.sdk.android.Fabric;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;


/**
 * Created by unai.masa on 05/04/2016.
 * General context instance.
 */
public class UnaiMasaBlogApp extends Application {

    private static final Logger sLogger = LoggerFactory.getLogger(UnaiMasaBlogApp.class);

    private static UnaiMasaBlogApp instance;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        sLogger.info("Application created - {}", new Date().getTime());
        instance = this;
        ImageLoaderConfigurator.setup(this);
    }

    public static Context getInstance() {
        return instance.getApplicationContext();
    }

    public static UnaiMasaBlogApp get() {
        return instance;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        ImageLoaderConfigurator.clearMemoryCache();
    }

}

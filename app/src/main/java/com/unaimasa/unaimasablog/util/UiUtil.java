package com.unaimasa.unaimasablog.util;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.unaimasa.unaimasablog.UnaiMasaBlogApp;

/**
 * Created by Dmitry Rusak on 7/14/15.
 * <p/>
 */
public class UiUtil {

    public static void hideSoftKeyboard(Context context, View view) {
        if (view != null) {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showSoftKeyboardForView(Context context, View view) {
        if (view != null) {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_FORCED);
        }
    }

    public static float getDeviceWidthInDp() {
        return (getDeviceWidthInPx() / getDisplayDensity());
    }

    public static float getDeviceHeightInDp() {
        return (getDeviceHeightInPx() / getDisplayDensity());
    }

    public static float getDeviceWidthInPx() {
        return (getDisplayMetrics().widthPixels);
    }

    public static float getDeviceHeightInPx() {
        return (getDisplayMetrics().heightPixels);
    }

    public static DisplayMetrics getDisplayMetrics() {
        return UnaiMasaBlogApp.getInstance().getResources().getDisplayMetrics();
    }

    public static float getDisplayDensity() {
        return UnaiMasaBlogApp.getInstance().getResources().getDisplayMetrics().density;
    }

    public static int convertDpToPx(float dp) {
        return (int) (dp * getDisplayDensity());
    }
}

package com.unaimasa.unaimasablog.util;

import android.support.annotation.StringRes;
import android.widget.Toast;

import com.unaimasa.unaimasablog.UnaiMasaBlogApp;
import com.unaimasa.unaimasablog.base.ui.view.SingleToast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by unai.masa on 08/04/2016.
 */
public class CommonUtils {

    public static void showToast(String text, int duration) { SingleToast.show(text, duration); }

    public static void showToast(@StringRes int resId, int duration) {
        SingleToast.show(UnaiMasaBlogApp.getInstance().getText(resId), duration);
    }

    public static void showToast(String text) {
        showToast(text, Toast.LENGTH_SHORT);
    }

    public static void showToast(int resId) {
        showToast(resId, Toast.LENGTH_SHORT);
    }

    public static boolean isEmpty(Collection<?> collection) {
        boolean result = false;
        if (collection == null || collection.isEmpty()) {
            result = true;
        }
        return result;
    }

    public static <T> Class<T> getGenericClass(Collection<T> collection) {
        if (isEmpty(collection)) {
            throw new IllegalArgumentException("add() " + ((collection == null) ? "null" : "empty") + " collection");
        }
        return (Class<T>) collection.iterator().next().getClass();

    }

    public static <T> List<T> getNewArrayList() {
        return new ArrayList<T>();
    }

}

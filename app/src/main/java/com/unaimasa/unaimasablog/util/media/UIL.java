package com.unaimasa.unaimasablog.util.media;

import android.graphics.Bitmap;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.unaimasa.unaimasablog.Constants;
import com.unaimasa.unaimasablog.util.UiUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * Created by Alexandr.Salin on 7/13/15.
 */
public class UIL {
    private static final Logger logger = LoggerFactory.getLogger(UIL.class);
    private final DisplayImageOptions.Builder optionsBuilder;
    private final String url;
    private final ImageView imageView;
    private boolean shouldResize = false;
    private ImageLoadingListener loadingListener;
    private ImageLoadingProgressListener progressListener;
    private int heightPx = -1; //mean use screen size
    private int widthPx = -1; //mean use screen size

    @NonNull
    public static UIL bind(String url, ImageView imageView) {
        return new UIL(url, imageView);
    }

    @NonNull
    public static UIL bind(String url, ImageView imageView, DisplayImageOptions.Builder defBuilder) {
        return new UIL(url, imageView, defBuilder);
    }

    private UIL(String url, ImageView imageView) {
        this(url, imageView, ImageLoaderConfigurator.getDefaultOptionsBuilder());
    }

    private UIL(String url, ImageView imageView, DisplayImageOptions.Builder defBuilder) {
        this.url = url;
        this.imageView = imageView;
        this.optionsBuilder = defBuilder;
    }

    public UIL onAllSteps(@DrawableRes int defImageRes) {
        optionsBuilder.showImageForEmptyUri(defImageRes);
        optionsBuilder.showImageOnFail(defImageRes);
        optionsBuilder.showImageOnLoading(defImageRes);
        return this;
    }

    public UIL setLoadingListener(ImageLoadingListener loadingListener) {
        this.loadingListener = loadingListener;
        return this;
    }

    public UIL setWidthPx(int widthPx) {
        shouldResize = true;
        this.widthPx = widthPx;
        return this;
    }

    public UIL setHeightPx(int heightPx) {
        shouldResize = true;
        this.heightPx = heightPx;
        return this;
    }

    public UIL setWidthDp(float widthDp) {
        setWidthPx(UiUtil.convertDpToPx(widthDp));
        return this;
    }

    public UIL setHeightDp(float heightDp) {
        setHeightPx(UiUtil.convertDpToPx(heightDp));
        return this;
    }

    public UIL setShouldResize(boolean shouldResize) {
        this.shouldResize = shouldResize;
        return this;
    }

    public UIL setAlpha(float alpha) {
        this.imageView.setAlpha(alpha);
        return this;
    }

    public String load() {
        String realImageUrl = shouldResize ? buildRealUrl(url) : url;
        if (loadingListener == null) {
            loadingListener = new PicPlayImageListener();
        }
        if (progressListener == null) {
            progressListener = new PicPlayImageLoaderProgressListener();
        }
        ImageLoader.getInstance()
                .displayImage(realImageUrl, imageView, optionsBuilder.build(), loadingListener, progressListener);
        return realImageUrl;
    }

    public String buildRealUrl(String url) {
        prepareSize();
        String postfix = Constants.EMPTY_STRING.concat("_").concat(Constants.EMPTY_STRING + widthPx).concat("x").concat(Constants.EMPTY_STRING + heightPx);
        int dotIndex = url.lastIndexOf(".");
        return url.substring(0, dotIndex) + postfix + url.substring(dotIndex, url.length());
    }

    /**
     * prepare correct values for width and height if we should force resize image
     * use screen size if value not set up
     */
    private void prepareSize() {
        if (widthPx == -1) {
            setWidthDp(UiUtil.getDeviceWidthInDp());
        }
        if (heightPx == -1) {
            setHeightDp(UiUtil.getDeviceHeightInDp());
        }
    }

    private static class PicPlayImageListener implements ImageLoadingListener {

        @Override
        public void onLoadingStarted(String imageUri, View view) {
            logger.warn("Loading starting " + imageUri);
        }

        @Override
        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            logger.warn("Loading failed " + imageUri, failReason);
        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            logger.warn("Loading completed " + imageUri);
        }

        @Override
        public void onLoadingCancelled(String imageUri, View view) {
            logger.warn("Loading cancelled " + imageUri);
        }
    }

    private static class PicPlayImageLoaderProgressListener implements ImageLoadingProgressListener {
        @Override
        public void onProgressUpdate(String imageUri, View view, int current, int total) {
//            logger.warn("Loading progress url: {}, current: {}, total: {}" + imageUri, current, total);
        }
    }

    public static void deleteSingleImageFromCache(String imageUrl) {
        File imageFile = ImageLoader.getInstance().getDiskCache().get(imageUrl);
        if (imageFile.exists()) {
            imageFile.delete();
        }
        MemoryCacheUtils.removeFromCache(imageUrl, ImageLoader.getInstance().getMemoryCache());
    }
}

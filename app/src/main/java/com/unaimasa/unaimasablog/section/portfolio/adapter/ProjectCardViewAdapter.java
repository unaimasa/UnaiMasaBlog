package com.unaimasa.unaimasablog.section.portfolio.adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.UnaiMasaBlogApp;
import com.unaimasa.unaimasablog.entity.Project;
import com.unaimasa.unaimasablog.util.CommonUtils;
import com.unaimasa.unaimasablog.util.media.UIL;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by unai.masa on 06/06/2016.
 */
public class ProjectCardViewAdapter extends RecyclerView.Adapter<ProjectCardViewAdapter.ProjectCardViewHolder> {

    private ArrayList<Project> mPortfolioProjectCollection;

    public ProjectCardViewAdapter(ArrayList<Project> portfolioProjectCollection) {
        this.mPortfolioProjectCollection = portfolioProjectCollection;
    }

    @Override
    public ProjectCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_portfolio_project_card_view, parent, false);

        return new ProjectCardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProjectCardViewHolder holder, int position) {
        final Project project = mPortfolioProjectCollection.get(position);
        // Set Project Link On Click
        holder.mRLProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setGooglePlayProjectLink(project.getLink());
            }
        });

        // Set Project Image
        UIL.bind(project.getImageUrl(), holder.mIVProject).load();
        // Set Project Title
        holder.mTVProjectTitle.setText(project.getTitle());
        Project.ProjectPlatform platform = project.getProjectPlatform();
        switch (platform) {
            case ANDROID:
                holder.mIVAndroid.setVisibility(View.VISIBLE);
                break;
            case APPLE:
                holder.mIVApple.setVisibility(View.VISIBLE);
                break;
            case BOTH:
                holder.mIVAndroid.setVisibility(View.VISIBLE);
                holder.mIVApple.setVisibility(View.VISIBLE);
                break;
            case MULTIPLATFORM:
                holder.mIVAndroid.setVisibility(View.VISIBLE);
                holder.mIVApple.setVisibility(View.VISIBLE);
                holder.mIVHtml.setVisibility(View.VISIBLE);
                break;
        }

    }

    @Override
    public int getItemCount() {
        return mPortfolioProjectCollection.size();
    }

    private void setGooglePlayProjectLink(String packageName){
        final String appPackageName = packageName;
        if(!packageName.isEmpty()) {
            try {
                UnaiMasaBlogApp.getInstance().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            } catch (android.content.ActivityNotFoundException anfe) {
                UnaiMasaBlogApp.getInstance().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        }else{
            CommonUtils.showToast(R.string.error_no_link_available);
        }
    }

    public static class ProjectCardViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.cv_container)
        RelativeLayout mRLProject;

        @Bind(R.id.iv_project)
        ImageView mIVProject;

        @Bind(R.id.tv_project_title)
        TextView mTVProjectTitle;

        @Bind(R.id.iv_android)
        ImageView mIVAndroid;

        @Bind(R.id.iv_apple)
        ImageView mIVApple;

        @Bind(R.id.iv_html)
        ImageView mIVHtml;

        public ProjectCardViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}

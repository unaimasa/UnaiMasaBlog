package com.unaimasa.unaimasablog.section.settings.controller;

import android.content.Context;
import android.view.View;

import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.base.ui.fragment.BaseFragment;
import com.unaimasa.unaimasablog.section.settings.fragment.options.HelpFaqSettingsFragment;
import com.unaimasa.unaimasablog.section.settings.fragment.options.LanguageSettingsFragment;
import com.unaimasa.unaimasablog.section.settings.fragment.options.PrivacyPolicySettingsFragment;
import com.unaimasa.unaimasablog.section.settings.fragment.SettingsFragment;
import com.unaimasa.unaimasablog.section.settings.fragment.options.TermsConditionsSettingsFragment;
import com.unaimasa.unaimasablog.util.CommonUtils;

/**
 * Created by unai.masa on 18/04/2016.
 */
public class SettingItemController implements SettingsFragment.SettingItemListener{

    private Context mContext;
    private SettingsFragment.SettingDataChangedListener mDataChangedListener;

    public SettingItemController(Context context, SettingsFragment.SettingDataChangedListener dataChangedListener) {
        mContext = context;
        mDataChangedListener = dataChangedListener;
    }

    @Override
    public void onHelpClicked() {
        BaseFragment fragment = new HelpFaqSettingsFragment();
        mDataChangedListener.addSettingFragment(fragment);
    }

    @Override
    public void onPrivacyClicked() {
        BaseFragment fragment = new PrivacyPolicySettingsFragment();
        mDataChangedListener.addSettingFragment(fragment);
    }

    @Override
    public void onTermsClicked() {
        BaseFragment fragment = new TermsConditionsSettingsFragment();
        mDataChangedListener.addSettingFragment(fragment);
    }

    @Override
    public void onLanguageClicked() {
        BaseFragment fragment = new LanguageSettingsFragment();
        mDataChangedListener.addSettingFragment(fragment);
    }

    @Override
    public void onNotificationClicked(View toggledView, boolean isOn) {
        // SharedPreferencesUtil.getInstance().putBool(Constants.SharedKeys.USER_NOTIFICATION, toggledView.isActivated());
        CommonUtils.showToast(R.string.error_generic);
    }

}

package com.unaimasa.unaimasablog.section.settings.fragment.options;

import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.section.settings.fragment.BaseHtmlSettingsFragment;

/**
 * Created by unai.masa on 18/04/2016.
 */
public class PrivacyPolicySettingsFragment extends BaseHtmlSettingsFragment {

    @Override
    protected String getScreenTitle() {
        return getString(R.string.privacy_policy);
    }

    @Override
    protected String getContentUrl() {
        return getString(R.string.path_raw_html_privacy);
    }

}

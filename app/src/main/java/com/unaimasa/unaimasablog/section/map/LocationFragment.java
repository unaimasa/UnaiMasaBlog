package com.unaimasa.unaimasablog.section.map;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.base.ui.fragment.BaseFragment;
import com.unaimasa.unaimasablog.section.map.adapter.MapTypeSpinnerAdapter;
import com.unaimasa.unaimasablog.section.map.helper.MapHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import butterknife.Bind;

/**
 * Created by unai.masa on 03/05/2016.
 */
public class LocationFragment extends BaseFragment implements OnMapReadyCallback {

    private static final Logger sLogger = LoggerFactory.getLogger(LocationFragment.class);

    protected Toolbar mToolbar;
    protected Menu mOptionsMenu;

    // ===== Spinner Elements ===== //
    @Bind(R.id.map_types_spinner)
    Spinner mMapTypesSpinner;

    private MapTypeSpinnerAdapter mMapTypeSpinnerAdapter;

    private String[] types_array;

    // ===== Map Elements ===== //
    private GoogleMap mMap;
    private MapHelper mMapHelper;

    @Override
    protected void afterInject(View rootView, Bundle savedInstanceState) {

        // Put Toolbar Title
        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.drawer_location_text);

        // Get Toolbar Menu
        mOptionsMenu = mToolbar.getMenu();

        // Hide Home Button
        setHomeToolbar(mOptionsMenu);

        // Set Spinner Adapter
        types_array = getActivity().getResources().getStringArray(R.array.map_type_array);
        mMapTypeSpinnerAdapter = new MapTypeSpinnerAdapter(getActivity(), R.layout.item_map_type_list, R.id.tv_map_type, types_array);
        mMapTypesSpinner.setAdapter(mMapTypeSpinnerAdapter);
        addMapTypesSpinnerListener(mMapTypesSpinner);

        // Get Map Fragment
        SupportMapFragment mMapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.location_map);

        // Add Map Listener
        mMapFragment.getMapAsync(this);

    }

    @Override
    protected int getCustomLayoutId() { return R.layout.fragment_map; }

    // ===== Spinner Methods ===== //
    private void addMapTypesSpinnerListener(Spinner spinner){
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                if(mMap != null) {
                    mMapHelper.setMapType(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    // ===== OnMapReady Methods ===== //
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Set Map Helper
        mMapHelper = new MapHelper(mMap, mMapTypesSpinner);
    }
}

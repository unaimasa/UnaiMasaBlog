package com.unaimasa.unaimasablog.section.settings.fragment.options;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.unaimasa.unaimasablog.Constants;
import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.base.ui.fragment.BaseFragment;
import com.unaimasa.unaimasablog.section.blog.fragment.BlogFragment;
import com.unaimasa.unaimasablog.section.settings.adapter.LanguageListAdapter;
import com.unaimasa.unaimasablog.section.settings.listener.LanguageListListener;
import com.unaimasa.unaimasablog.util.media.UIL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;

import butterknife.Bind;

/**
 * Created by unai.masa on 15/04/2016.
 */
public class LanguageSettingsFragment extends BaseFragment implements LanguageListListener {

    private static final Logger sLogger = LoggerFactory.getLogger(LanguageSettingsFragment.class);

    // ===== Language Elements ===== //
    @Bind(R.id.lv_languages)
    protected ListView mLvLanguages;

    private LanguageListAdapter mLanguageListAdapter;

    protected Toolbar mToolbar;
    protected Menu mOptionsMenu;

    @Override
    protected void afterInject(View rootView, Bundle savedInstanceState) {

        // Put Toolbar Title
        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.language_title);

        // Get Toolbar Menu
        mOptionsMenu = mToolbar.getMenu();

        // Hide Home Button
        hideHomeToolbar(mOptionsMenu);

        // Set ListView Adapter
        mLanguageListAdapter = new LanguageListAdapter(getActivity(), R.layout.item_language_list, R.id.tv_li_language_name, getResources().getStringArray(R.array.language_list), this);
        mLvLanguages.setAdapter(mLanguageListAdapter);

    }

    @Override
    public void onResume(){
        super.onResume();

        // Put Toolbar Title
        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.language_title);
    }

    @Override
    protected int getCustomLayoutId() { return R.layout.fragment_language; }

    // ===== Language List Listener Methods ===== //
    @Override
    public void languageSelected(String keyCode) { setLocale(keyCode); }

    // ===== Language Methods ===== //
    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        // Refresh the activity and go to the Main Fragment
        getActivity().recreate();
    }

}

package com.unaimasa.unaimasablog.section.map.helper;

import android.widget.Spinner;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.unaimasa.unaimasablog.Constants;
import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.entity.MapMarker;
import com.unaimasa.unaimasablog.util.SharedPreferencesUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * Created by unai.masa on 08/06/2016.
 */
public class MapHelper{

    private static final Logger sLogger = LoggerFactory.getLogger(MapHelper.class);

    private static int GOOGLE_MAP_PADDING = 100;

    private Spinner mMapTypesSpinner;

    // ===== Map Elements ===== //
    private GoogleMap mMap;
    private ArrayList<MapMarker> mMapMarkers;

    /**
     * Constructor for Map Helper
     * @param map LocationFragment to get the context
     * @param spinner Spinner to get the context
     */
    public MapHelper(GoogleMap map, Spinner spinner) {
        this.mMap = map;
        this.mMapTypesSpinner = spinner;
        onMapReady(mMap);
    }

    // ===== OnMapReady Methods ===== //
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Put Map Type
        int type = SharedPreferencesUtil.getInstance().getInt(Constants.SharedKeys.MAP_TYPE, 0);
        mMapTypesSpinner.setSelection(type);

        // Create Array List of Markers
        mMapMarkers = new ArrayList<MapMarker>();
        mMapMarkers = getMapMarkers();
        BitmapDescriptor iconVisited = BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_place_visited);
        BitmapDescriptor iconCurrent = BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_current_position);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        for (MapMarker marker : mMapMarkers) {
            String markerLocation = marker.getLocation();
            LatLng position = marker.getPosition();
            switch (marker.getMarkerType()) {
                case CURRENT:
                    mMap.addMarker(new MarkerOptions()
                            .position(position)
                            .icon(iconCurrent)
                            .title(markerLocation));
                    break;
                case VISITED:
                    mMap.addMarker(new MarkerOptions()
                            .position(position)
                            .icon(iconVisited)
                            .title(markerLocation));
                    break;
            }

            builder.include(position);
        }

        try {
            LatLngBounds bounds = builder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, GOOGLE_MAP_PADDING);
            mMap.animateCamera(cu);
        } catch (Exception e) {
            LatLng defaultPosition = new LatLng(0, 0);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(defaultPosition));
        }
    }

    // ===== Set Map Type ===== //
    public void setMapType(int type){
        if(mMap != null) {
            switch (type) {
                case 0:
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    break;
                case 1:
                    mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                    break;
                case 2:
                    mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                    break;
                case 3:
                    mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                    break;
                case 4:
                    mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                    break;
                default:
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    break;
            }
        }
        SharedPreferencesUtil.getInstance().putInt(Constants.SharedKeys.MAP_TYPE, type);
    }

    // ===== Create Array List of Project ===== //
    private ArrayList<MapMarker> getMapMarkers(){
        mMapMarkers.clear();

        // Spain
        MapMarker bilbao = new MapMarker("Bilbao", 43.263018, -2.934987, MapMarker.MarkerType.VISITED);
        MapMarker santander = new MapMarker("Santander", 43.462338, -3.809985, MapMarker.MarkerType.VISITED);
        MapMarker madrid = new MapMarker("Madrid", 40.416929, -3.703556, MapMarker.MarkerType.VISITED);

        // France
        MapMarker paris = new MapMarker("París", 48.856484, 2.352385, MapMarker.MarkerType.VISITED);

        // UK
        MapMarker london = new MapMarker("London", 51.50735, -0.127758, MapMarker.MarkerType.CURRENT);

        // Italy
        MapMarker cerdena = new MapMarker("Cerdeña", 40.115059, 9.005878, MapMarker.MarkerType.VISITED);
        MapMarker napoles = new MapMarker("Napoles", 40.850848, 14.268916, MapMarker.MarkerType.VISITED);
        MapMarker roma = new MapMarker("Roma", 41.901809, 12.498911, MapMarker.MarkerType.VISITED);
        MapMarker pisa = new MapMarker("Pisa", 43.722379, 10.400528, MapMarker.MarkerType.VISITED);
        MapMarker florencia = new MapMarker("Florencia", 43.770021, 11.259849, MapMarker.MarkerType.VISITED);

        // Monaco
        MapMarker monaco = new MapMarker("Monaco", 43.737949, 7.422429, MapMarker.MarkerType.VISITED);

        // EE.UU.
        MapMarker new_york = new MapMarker("Nueva York", 40.712770, -74.006103, MapMarker.MarkerType.VISITED);
        MapMarker washington = new MapMarker("Washington", 38.907251, -77.036552, MapMarker.MarkerType.VISITED);
        MapMarker miami = new MapMarker("Miami", 25.761835, -80.191697, MapMarker.MarkerType.VISITED);


        mMapMarkers.add(bilbao);
        mMapMarkers.add(santander);
        mMapMarkers.add(madrid);
        mMapMarkers.add(paris);
        mMapMarkers.add(london);
        mMapMarkers.add(cerdena);
        mMapMarkers.add(napoles);
        mMapMarkers.add(roma);
        mMapMarkers.add(pisa);
        mMapMarkers.add(florencia);
        mMapMarkers.add(monaco);
        mMapMarkers.add(new_york);
        mMapMarkers.add(washington);
        mMapMarkers.add(miami);

        return mMapMarkers;
    }

}

package com.unaimasa.unaimasablog.section.about;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.unaimasa.unaimasablog.Constants;
import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.base.ui.fragment.BaseFragment;
import com.unaimasa.unaimasablog.util.media.UIL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import butterknife.Bind;

/**
 * Created by unai.masa on 15/04/2016.
 */
public class AboutFragment extends BaseFragment {

    private static final Logger sLogger = LoggerFactory.getLogger(AboutFragment.class);

    // ===== About Elements ===== //
    @Bind(R.id.iv_about_image_1)
    protected ImageView mIVAbout1;

    @Bind(R.id.tv_about_text_1)
    protected TextView mTVAboutText1;

    protected Toolbar mToolbar;
    protected Menu mOptionsMenu;

    @Override
    protected void afterInject(View rootView, Bundle savedInstanceState) {

        // Put Toolbar Title
        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.drawer_about_text);

        // Get Toolbar Menu
        mOptionsMenu = mToolbar.getMenu();

        // Hide Home Button
        setHomeToolbar(mOptionsMenu);

        // Load About Image
        UIL.bind(Constants.AppPhotos.ABOUT_PHOTO, mIVAbout1).setLoadingListener(mImageLoadingListener).load();

    }

    @Override
    protected int getCustomLayoutId() { return R.layout.fragment_about; }

    // ===== Universal Image Loader Methods ===== //

    private ImageLoadingListener mImageLoadingListener = new SimpleImageLoadingListener() {
        @Override
        public void onLoadingStarted(String imageUri, View view) { }

        @Override
        public void onLoadingFailed(String imageUri, View view, FailReason failReason) { }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) { }
    };

}

package com.unaimasa.unaimasablog.section.blog.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.youtube.player.YouTubeIntents;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.unaimasa.unaimasablog.BuildConfig;
import com.unaimasa.unaimasablog.Constants;
import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.UnaiMasaBlogApp;
import com.unaimasa.unaimasablog.base.UriResolver;
import com.unaimasa.unaimasablog.base.listener.UnaiMasaBlogScrollViewListener;
import com.unaimasa.unaimasablog.base.listener.UnaiMasaBlogYouTubeThumbnailViewListener;
import com.unaimasa.unaimasablog.base.ui.fragment.BaseFragment;
import com.unaimasa.unaimasablog.base.ui.view.UnaiMasaBlogScrollView;
import com.unaimasa.unaimasablog.base.ui.view.UnaiMasaBlogYouTubeThumbnailView;
import com.unaimasa.unaimasablog.entity.rest.Blogmedia;
import com.unaimasa.unaimasablog.entity.rest.BlogmediaPublishedCollection;
import com.unaimasa.unaimasablog.rest.IWebApi;
import com.unaimasa.unaimasablog.section.blog.ui.BlogFloatingActionButton;
import com.unaimasa.unaimasablog.section.blog.adapter.BlogPostSpinnerAdapter;
import com.unaimasa.unaimasablog.section.blog.callback.BlogCallbacks;
import com.unaimasa.unaimasablog.util.SharedPreferencesUtil;
import com.unaimasa.unaimasablog.util.media.UIL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by unai.masa on 13/04/2016.
 */
public class BlogFragment extends BaseFragment implements Callback<BlogmediaPublishedCollection>, UnaiMasaBlogScrollViewListener, UnaiMasaBlogYouTubeThumbnailViewListener {

    private static final Logger sLogger = LoggerFactory.getLogger(BlogFragment.class);
    final static String RELEASE_BUILD_TYPE = "release";

    // ===== ScrollView ===== //
    @Bind(R.id.sv_blog_post)
    protected UnaiMasaBlogScrollView mScrollView;

    // ===== Spinner ===== //
    @Bind(R.id.spr_blog_post)
    protected Spinner mBlogPostSpinner;

    private BlogPostSpinnerAdapter mBlogPostSpinnerAdapter;

    // ===== Blog Post Elements ===== //
    @Bind(R.id.tv_blog_title)
    protected TextView mTVBlogTitle;

    @Bind(R.id.tv_blog_date)
    protected TextView mTVBlogDate;

    @Bind(R.id.iv_youtube_thumbnail)
    protected YouTubeThumbnailView mIVYoutubeThumbnail;

    @Bind(R.id.tv_blog_text_1)
    protected TextView mTVBlogText1;

    @Bind(R.id.adView)
    protected AdView mAdView;

    @Bind(R.id.tv_blog_subtitle)
    protected TextView mTVBlogSubtitle;

    @Bind(R.id.iv_imageview)
    protected ImageView mIVBlogImage;

    @Bind(R.id.tv_blog_text_2)
    protected TextView mTVBlogText2;

    @Bind(R.id.tv_blog_greetings)
    protected TextView mTVBlogGreetings;

    @Bind(R.id.fab_blog_folder)
    protected BlogFloatingActionButton mFABBlogFolder;

    // private YouTubeThumbnailLoader mYouTubeThumbnailLoader;
    private UnaiMasaBlogYouTubeThumbnailView mUnaiMasaBlogYouTubeThumbnailView;

    protected BlogCallbacks mCallbacks;

    protected Toolbar mToolbar;
    protected Menu mOptionsMenu;

    protected TelephonyManager telephonyManager;

    // Blogmedia Published Collection
    private ArrayList<Blogmedia> mBlogmediaPublishedCollection = new ArrayList<Blogmedia>();
    protected Blogmedia mBlogmedia;

    // ===== Fragment Constructor ===== //
    public static BlogFragment newInstance() { return newInstance(null); }

    public static BlogFragment newInstance(Bundle extraArgs) {
        BlogFragment fragment = new BlogFragment();
        if (extraArgs != null) {
            fragment.setArguments(extraArgs);
        }
        return fragment;
    }

    public BlogFragment() { }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Initialize AdMob
        MobileAds.initialize(UnaiMasaBlogApp.getInstance(), Constants.AdMob_Configuration.APP_ID);
    }

    @Override
    protected void afterInject(View rootView, Bundle savedInstanceState) {

        // Put Toolbar Title
        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.app_name);

        // Get Toolbar Menu
        mOptionsMenu = mToolbar.getMenu();

        // Hide Home Button
        showHomeButton(mOptionsMenu, false);

        // Set ScrollView Listener
        mScrollView.setScrollViewListener(this);

        // Initialize Youtube Thumbnail Listener
        mUnaiMasaBlogYouTubeThumbnailView = new UnaiMasaBlogYouTubeThumbnailView(this.getContext(), mIVYoutubeThumbnail);
        mUnaiMasaBlogYouTubeThumbnailView.setUnaiMasaBlogYouTubeThumbnailViewListener(this);

        // Initialize Blog Floating Action Button and set Spinner
        mFABBlogFolder = new BlogFloatingActionButton(getContext());
        mFABBlogFolder.setSpinner(mBlogPostSpinner);

        // Get Blogmedia Published Collection
        getBlogmediaPublishedCollection();

        // Get Telephony Manager
        telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        // Put AdMob Banner
        AdRequest adRequest;
        switch (BuildConfig.BUILD_TYPE) {
            case RELEASE_BUILD_TYPE:
                adRequest = new AdRequest.Builder().build();
                break;
            default:
                String deviceid = telephonyManager.getDeviceId();
                adRequest = new AdRequest.Builder()
                        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                        .addTestDevice(deviceid)
                        .build();
        }
        mAdView.loadAd(adRequest);
    }

    @Override
    protected int getCustomLayoutId() { return R.layout.fragment_blog; }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BlogCallbacks) {
            mCallbacks = (BlogCallbacks) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
        try {
            mUnaiMasaBlogYouTubeThumbnailView.releaseYouTubeThumbnailLoader();
        }catch (NullPointerException e) {

        }
    }

    @OnClick(R.id.fab_blog_folder)
    public void onFabClick() {
        mFABBlogFolder.setBlogPostSpinnerVisivility();
    }

    /**
     * Method to set BlogmediaPublishedCollection in the Fragment
     * @param blogmediaPublishedCollection : BlogmediaPublishedCollection to work with
     **/
    public void setBlogmediaPublishedCollection(ArrayList<Blogmedia> blogmediaPublishedCollection) {
        if(mBlogmediaPublishedCollection.size() == 0) {
            // Show Empty Blog Screen
        } else {
            int position = SharedPreferencesUtil.getInstance().getInt(Constants.SharedKeys.BLOG_POSITION, 0);
            mBlogPostSpinnerAdapter = new BlogPostSpinnerAdapter(getActivity(), R.layout.item_blog_post_list, R.id.tv_blog_title_spinner, blogmediaPublishedCollection);
            mBlogPostSpinner.setAdapter(mBlogPostSpinnerAdapter);
            mBlogPostSpinner.setSelection(position);
            addBlogPostSpinnerListener(mBlogPostSpinner);
            try {
                mBlogmedia = mBlogmediaPublishedCollection.get(position);
                setBlogmedia();
            }catch(IndexOutOfBoundsException exception) {
                // Position out of the ArrayList, show Empty Blog Screen
                // Reset blog position
                SharedPreferencesUtil.getInstance().putInt(Constants.SharedKeys.BLOG_POSITION, 0);
            }
        }
    }

    /**
     * Method to set selected Blogmedia in the Fragment
     **/
    public void setBlogmedia() {
        mTVBlogTitle.setText(Html.fromHtml(mBlogmedia.getTitle()));

        mUnaiMasaBlogYouTubeThumbnailView.setYouTubeThumbnailLoaderVideo(mBlogmedia.getVideoUrl());

        mTVBlogDate.setText(Html.fromHtml(mBlogmedia.getDate()));
        mTVBlogText1.setText(Html.fromHtml(mBlogmedia.getFirstParagraph()));
        mTVBlogSubtitle.setText(Html.fromHtml(mBlogmedia.getSubtitle()));

        // Load Image
        String imageUrl = mBlogmedia.getImageUrl();
        UIL.bind(imageUrl, mIVBlogImage).setLoadingListener(mImageLoadingListener).load();

        mTVBlogText2.setText(Html.fromHtml(mBlogmedia.getSecondParagraph()));
        mTVBlogGreetings.setText(Html.fromHtml(mBlogmedia.getThanksParagraph()));
    }

    // ===== ScrollView Methods ===== //
    @Override
    public void onScrollChanged(UnaiMasaBlogScrollView scrollView, int x, int y, int oldx, int oldy) {
        mBlogPostSpinner.setVisibility(View.GONE);
    }

    // ===== Youtube Thumbnail Methods ===== //
    @Override
    public void onInitializationSuccess() { if (mBlogmedia != null) mUnaiMasaBlogYouTubeThumbnailView.setYouTubeThumbnailLoaderVideo(mBlogmedia.getVideoUrl()); }

    @Override
    public void onInitializationFailure() { }

    @Override
    public void onThumbnailClick() {
        // If the user clicks in the thumbnail, open the video
        startActivity(YouTubeIntents.createPlayVideoIntentWithOptions(getActivity(), mBlogmedia.getVideoUrl(), true, true));
    }

    // ===== Spinner Methods ===== //
    private void addBlogPostSpinnerListener(Spinner spinner){
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                SharedPreferencesUtil.getInstance().putInt(Constants.SharedKeys.BLOG_POSITION, position);
                mBlogmedia = mBlogmediaPublishedCollection.get(position);
                setBlogmedia();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) { }
        });
    }

    // ===== Universal Image Loader Methods ===== //
    private ImageLoadingListener mImageLoadingListener = new SimpleImageLoadingListener() {
        @Override
        public void onLoadingStarted(String imageUri, View view) { }

        @Override
        public void onLoadingFailed(String imageUri, View view, FailReason failReason) { }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) { }
    };

    // ===== Retrofit ===== //
    public void getBlogmediaPublishedCollection(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.API_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // prepare call in Retrofit 2.0
        IWebApi iWebAPI = retrofit.create(IWebApi.class);

        String path = UriResolver.getBlogmediaPublishedResolver().toString() + "." + Constants.API_Return_Format.JSON;
        String etag = SharedPreferencesUtil.getInstance().getString(Constants.SharedKeys.ETAG_VALUE);
        Call<BlogmediaPublishedCollection> call = iWebAPI.getBlogmediaPublished(etag, path);

        // asynchronous call
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<BlogmediaPublishedCollection> call, Response<BlogmediaPublishedCollection> response) {
        if(response.isSuccessful()){
            mBlogmediaPublishedCollection = response.body().getItems();
            setBlogmediaPublishedCollection(mBlogmediaPublishedCollection);
        }
    }

    @Override
    public void onFailure(Call<BlogmediaPublishedCollection> call, Throwable t) { }

}

package com.unaimasa.unaimasablog.section.cv;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;

import com.unaimasa.unaimasablog.Constants;
import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.base.ui.fragment.BaseFragment;
import com.unaimasa.unaimasablog.util.media.UIL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import butterknife.Bind;

/**
 * Created by unai.masa on 15/04/2016.
 */
public class CVFragment extends BaseFragment {

    private static final Logger sLogger = LoggerFactory.getLogger(CVFragment.class);

    // ===== CV Elements ===== //
    @Bind(R.id.iv_skill_android)
    protected ImageView mIVSkillAndroid;

    @Bind(R.id.iv_skill_html5)
    protected ImageView mIVSkillHtml5;

    @Bind(R.id.iv_skill_javascript)
    protected ImageView mIVSkillJavascript;

    @Bind(R.id.iv_skill_phonegap)
    protected ImageView mIVSkillPhonegap;

    @Bind(R.id.iv_skill_jquery)
    protected ImageView mIVSkillJquery;

    @Bind(R.id.iv_skill_php)
    protected ImageView mIVSkillPhp;

    @Bind(R.id.iv_skill_mysql)
    protected ImageView mIVSkillMysql;

    @Bind(R.id.iv_skill_scrum)
    protected ImageView mIVSkillScrum;

    @Bind(R.id.iv_skill_pomodoro)
    protected ImageView mIVSkillPomodoro;

    @Bind(R.id.iv_skill_ios)
    protected ImageView mIVSkillIos;

    @Bind(R.id.iv_skill_python)
    protected ImageView mIVSkillPython;

    @Bind(R.id.iv_skill_cplusplus)
    protected ImageView mIVSkillCPlusPlus;

    @Bind(R.id.iv_experience_picsolve)
    protected ImageView mIVExperiencePicsolve;

    @Bind(R.id.iv_experience_tst)
    protected ImageView mIVExperienceTst;

    @Bind(R.id.iv_experience_byv)
    protected ImageView mIVExperienceByv;

    @Bind(R.id.iv_experience_batura)
    protected ImageView mIVExperienceBatura;

    @Bind(R.id.iv_study_degree_1)
    protected ImageView mIVStudyDegree1;

    @Bind(R.id.iv_study_degree_2)
    protected ImageView mIVStudyDegree2;

    @Bind(R.id.iv_study_master_1)
    protected ImageView mIVStudyMaster1;

    protected Toolbar mToolbar;
    protected Menu mOptionsMenu;

    @Override
    protected void afterInject(View rootView, Bundle savedInstanceState) {

        // Put Toolbar Title
        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.drawer_cv_text);

        // Get Toolbar Menu
        mOptionsMenu = mToolbar.getMenu();

        // Hide Home Button
        setHomeToolbar(mOptionsMenu);

        // Load Skills Images
        UIL.bind(Constants.AppPhotos.CV_SKILL_ANDROID, mIVSkillAndroid).load();
        UIL.bind(Constants.AppPhotos.CV_SKILL_HTML5, mIVSkillHtml5).load();
        UIL.bind(Constants.AppPhotos.CV_SKILL_JAVASCRIPT, mIVSkillJavascript).load();
        UIL.bind(Constants.AppPhotos.CV_SKILL_PHONEGAP, mIVSkillPhonegap).load();
        UIL.bind(Constants.AppPhotos.CV_SKILL_JQUERY, mIVSkillJquery).load();
        UIL.bind(Constants.AppPhotos.CV_SKILL_PHP, mIVSkillPhp).load();
        UIL.bind(Constants.AppPhotos.CV_SKILL_MYSQL, mIVSkillMysql).load();
        UIL.bind(Constants.AppPhotos.CV_SKILL_SCRUM, mIVSkillScrum).load();
        UIL.bind(Constants.AppPhotos.CV_SKILL_POMODORO, mIVSkillPomodoro).load();
        UIL.bind(Constants.AppPhotos.CV_SKILL_IOS, mIVSkillIos).load();
        UIL.bind(Constants.AppPhotos.CV_SKILL_PYTHON, mIVSkillPython).load();
        UIL.bind(Constants.AppPhotos.CV_SKILL_CPLUSPLUS, mIVSkillCPlusPlus).load();
        UIL.bind(Constants.AppPhotos.CV_EXPERIENCE_PICSOLVE, mIVExperiencePicsolve).load();
        UIL.bind(Constants.AppPhotos.CV_EXPERIENCE_TST, mIVExperienceTst).load();
        UIL.bind(Constants.AppPhotos.CV_EXPERIENCE_BYV, mIVExperienceByv).load();
        UIL.bind(Constants.AppPhotos.CV_EXPERIENCE_BATURA, mIVExperienceBatura).load();
        UIL.bind(Constants.AppPhotos.CV_STUDY_EHU, mIVStudyDegree1).load();
        UIL.bind(Constants.AppPhotos.CV_STUDY_EHU, mIVStudyDegree2).load();
        UIL.bind(Constants.AppPhotos.CV_STUDY_EHU, mIVStudyMaster1).load();


    }

    @Override
    protected int getCustomLayoutId() { return R.layout.fragment_cv; }

}

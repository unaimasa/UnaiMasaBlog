package com.unaimasa.unaimasablog.section.settings.listener;

/**
 * Created by unai.masa on 17/06/2016.
 */
public interface LanguageListListener {
    void languageSelected(String keyCode);
}

package com.unaimasa.unaimasablog.section.splash.background;

import android.app.Activity;
import android.os.AsyncTask;

import at.grabner.circleprogress.CircleProgressView;

/**
 * Created by unai.masa on 14/06/2016.
 */
public class CircleProgressViewOperation extends AsyncTask<Void, Void, Void> {

    private Activity mActivity;
    private CircleProgressView mCircleProgressView;

    public CircleProgressViewOperation(Activity activity, CircleProgressView circleProgressView) {
        this.mActivity = activity;
        this.mCircleProgressView = circleProgressView;
    }

    @Override
    protected Void doInBackground(Void... params) {

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCircleProgressView.setValue(0);
                mCircleProgressView.spin();
            }
        });

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        mCircleProgressView.setValueAnimated(100);
    }
}
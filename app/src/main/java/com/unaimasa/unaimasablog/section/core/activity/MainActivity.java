package com.unaimasa.unaimasablog.section.core.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.unaimasa.unaimasablog.Constants;
import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.base.ui.activity.BaseActivity;
import com.unaimasa.unaimasablog.section.about.AboutFragment;
import com.unaimasa.unaimasablog.section.blog.fragment.BlogFragment;
import com.unaimasa.unaimasablog.section.cv.CVFragment;
import com.unaimasa.unaimasablog.section.map.LocationFragment;
import com.unaimasa.unaimasablog.section.portfolio.PortfolioFragment;
import com.unaimasa.unaimasablog.section.settings.fragment.SettingsFragment;
import com.unaimasa.unaimasablog.social.SocialNetworkIntent;
import com.unaimasa.unaimasablog.util.SharedPreferencesUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final Logger sLogger = LoggerFactory.getLogger(MainActivity.class);

    // ===== Toolbar ===== //
    @Bind(R.id.toolbar)
    protected Toolbar toolbar;

    // ===== Drawer ===== //
    @Bind(R.id.drawer_layout)
    protected DrawerLayout drawer;

    // ===== Navigation View ===== //
    @Bind(R.id.nav_view)
    protected NavigationView navigationView;

    // ===== Navigation View ===== //
    Menu optionsMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the Language
        String lang = SharedPreferencesUtil.getInstance().getString(Constants.SharedKeys.LANGUAGE_SELECTED_CODE, Locale.getDefault().getLanguage());
        setLocale(lang);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();

        // Add the Toolbar
        setSupportActionBar(toolbar);

        // Add the Drawer
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        // Add Navigation View
        navigationView.setNavigationItemSelectedListener(this);

        // Start by default
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container_fragments);
        if (fragment == null) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container_fragments, new BlogFragment()).commit();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ImageLoader.getInstance().clearMemoryCache();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.optionsMenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_home:
                navigationView.getMenu().getItem(0).setChecked(true);
                getSupportFragmentManager().beginTransaction()
                        .disallowAddToBackStack()
                        .setCustomAnimations(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_left_exit)
                        .replace(R.id.container_fragments, new BlogFragment())
                        .commit();
                break;
            case R.id.action_close:
                showCloseButton(false);
                showHomeButton(true);
                getSupportFragmentManager().popBackStack();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch(id){
            case R.id.nav_blog:
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_left_exit)
                        .replace(R.id.container_fragments, new BlogFragment())
                        .disallowAddToBackStack()
                        .commit();
                break;
            case R.id.nav_portfolio:
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_left_exit)
                        .replace(R.id.container_fragments, new PortfolioFragment())
                        .disallowAddToBackStack()
                        .commit();
                break;
            case R.id.nav_cv:
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_left_exit)
                        .replace(R.id.container_fragments, new CVFragment())
                        .disallowAddToBackStack()
                        .commit();
                break;
            case R.id.nav_about:
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_left_exit)
                        .replace(R.id.container_fragments, new AboutFragment())
                        .disallowAddToBackStack()
                        .commit();
                break;
            case R.id.nav_location:
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_left_exit)
                        .replace(R.id.container_fragments, new LocationFragment())
                        .disallowAddToBackStack()
                        .commit();
                break;
            case R.id.nav_youtube:
                Intent intentYoutube = SocialNetworkIntent.newYoutubeProfileIntent(getPackageManager(), Constants.SocialNetworks.YOUTUBE_CHANNEL);
                startActivity(intentYoutube);
                break;
            case R.id.nav_instagram:
                Intent intentInstagram = SocialNetworkIntent.newInstagramProfileIntent(getPackageManager(), Constants.SocialNetworks.INSTAGRAM_CHANNEL);
                startActivity(intentInstagram);
                break;
            case R.id.nav_twitter:
                Intent intentTwitter = SocialNetworkIntent.newTwitterProfileIntent(getPackageManager(), Constants.SocialNetworks.TWITTER_CHANNEL);
                startActivity(intentTwitter);
                break;
            case R.id.nav_github:
                Intent intentGithub = SocialNetworkIntent.newGithubProfileIntent(getPackageManager(), Constants.SocialNetworks.GITHUB_CHANNEL);
                startActivity(intentGithub);
                break;
            case R.id.nav_linkedin:
                Intent intentLinkedin = SocialNetworkIntent.newLinkedinProfileIntent(getPackageManager(), Constants.SocialNetworks.LINKEDIN_CHANNEL);
                startActivity(intentLinkedin);
                break;
            case R.id.nav_settings:
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_left_exit)
                        .replace(R.id.container_fragments, new SettingsFragment())
                        .addToBackStack("SettingsFragment")
                        .commit();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    // ===== UI Methods ===== //
    public void showHomeButton(boolean show) {
        if(optionsMenu != null) {
            optionsMenu.setGroupVisible(R.id.main_menu_home_group, show);
        }
    }

    public void showCloseButton(boolean show) {
        if(optionsMenu != null) {
            optionsMenu.setGroupVisible(R.id.main_menu_close_group, show);
        }
    }

    // ===== Language Methods ===== //
    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}

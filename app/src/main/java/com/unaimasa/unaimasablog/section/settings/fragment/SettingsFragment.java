package com.unaimasa.unaimasablog.section.settings.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;

import com.unaimasa.unaimasablog.Constants;
import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.base.ui.fragment.BaseFragment;
import com.unaimasa.unaimasablog.section.settings.adapter.SettingsListAdapter;
import com.unaimasa.unaimasablog.section.settings.controller.SettingItemController;
import com.unaimasa.unaimasablog.util.SharedPreferencesUtil;
import com.unaimasa.unaimasablog.util.UiUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import butterknife.Bind;

/**
 * Created by unai.masa on 18/04/2016.
 */
public class SettingsFragment extends BaseFragment {

    private static final Logger sLogger = LoggerFactory.getLogger(SettingsFragment.class);

    public static final String EXTRA_SETTING_ITEM_IS_TOGGLED = "settingIsToggled";

    @Bind(R.id.lv_settings)
    protected ListView mLvSettings;

    protected SettingsListAdapter mAdapter;

    protected SettingItemListener mSettingItemListener;

    protected Toolbar mToolbar;
    protected Menu mOptionsMenu;

    // ===== Fragment Constructor ===== //
    public static SettingsFragment newInstance() { return newInstance(null); }

    public static SettingsFragment newInstance(Bundle extraArgs) {
        SettingsFragment fragment = new SettingsFragment();
        if (extraArgs != null) {
            fragment.setArguments(extraArgs);
        }
        return fragment;
    }

    public SettingsFragment() { }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSettingItemListener = new SettingItemController(getActivity(), mSettingDataChangedCallback);

    }

    @Override
    public void onStart() {
        super.onStart();
        invalidateItems();
    }

    @Override
    public void onResume() {
        super.onResume();
        UiUtil.hideSoftKeyboard(getActivity(), getView());
        invalidateItems();
    }

    @Override
    public void onDestroyView() { super.onDestroyView(); }

    @Override
    protected void afterInject(View rootView, Bundle savedInstanceState) {

        // Put Toolbar Title
        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.drawer_settings_text);

        // Get Toolbar Menu
        mOptionsMenu = mToolbar.getMenu();

        // Hide Home Button
        setHomeToolbar(mOptionsMenu);

        mAdapter = new SettingsListAdapter(getActivity());
        mLvSettings.setAdapter(mAdapter);
    }

    @Override
    protected int getCustomLayoutId() { return R.layout.fragment_settings; }

    public void invalidateItems() {
        mAdapter.clear();

        /*
        mAdapter.add(new Setting() {
            @Override
            public String getItemName() {
                return getString(R.string.help_faqs);
            }

            @Override
            public void onItemSelected(Bundle extra) {
                setSettingsToolbar(mOptionsMenu);
                mSettingItemListener.onHelpClicked();
            }
        });
        mAdapter.add(new Setting() {
            @Override
            public String getItemName() {
                return getString(R.string.privacy_policy);
            }

            @Override
            public void onItemSelected(Bundle extra) {
                setSettingsToolbar(mOptionsMenu);
                mSettingItemListener.onPrivacyClicked();
            }
        });
        mAdapter.add(new Setting() {
            @Override
            public String getItemName() {
                return getString(R.string.terms_conditions);
            }

            @Override
            public void onItemSelected(Bundle extra) {
                setSettingsToolbar(mOptionsMenu);
                mSettingItemListener.onTermsClicked();
            }
        });
        */
        mAdapter.add(new Setting() {
            @Override
            public String getItemName() {
                return getString(R.string.language_title);
            }

            @Override
            public void onItemSelected(Bundle extra) {
                mSettingItemListener.onLanguageClicked();
            }
        });
        mAdapter.add(new Setting(Setting.Type.TOGGLE) {
            @Override
            public String getItemName() {
                return getString(R.string.notification_consent);
            }

            @Override
            public void onItemToggleSelected(Bundle extra, View toggledView) {
                    boolean isToggled = SharedPreferencesUtil.getInstance().getBoolean(Constants.SharedKeys.USER_NOTIFICATION, true);
                    mSettingItemListener.onNotificationClicked(toggledView, isToggled);
            }

            @Override
            public boolean getItemToggleState() {
                return SharedPreferencesUtil.getInstance()
                        .getBoolean(Constants.SharedKeys.USER_NOTIFICATION, false);
            }
        });
        mAdapter.notifyDataSetChanged();
    }

    protected SettingDataChangedListener mSettingDataChangedCallback = new SettingDataChangedListener() {

        @Override
        public void onStartLoadingNewData() {
            showProgressFullScreen();
        }

        @Override
        public void onDataChanged() {
            invalidateItems();
        }

        @Override
        public void onEndLoadingNewData() { hideProgressFullScreen(); }

        @Override
        public void addSettingFragment(BaseFragment fragment) {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_fragments, fragment)
                    .addToBackStack(fragment.getClass().getName())
                    .commit();
        }
    };


    public interface SettingDataChangedListener {
        void onStartLoadingNewData();
        /**
         * this method is used for hiding progress bar
         */
        void onEndLoadingNewData();
        void onDataChanged();
        void addSettingFragment(BaseFragment fragment);
    }

    public static class Setting implements ISettingSelected {

        public enum Type {ITEM, BUTTON, TOGGLE}

        private final Type mType;

        public Setting(Type type) {
            mType = type;
        }

        public Setting() {
            this(Type.ITEM);
        }

        public Type getType() {
            return mType;
        }

        @Override
        public String getItemName() {
            return "";
        }

        @Override
        public String getItemSubName() {
            return null;
        }

        @Override
        public void onItemSelected(Bundle extra) { }

        @Override
        public void onItemToggleSelected(Bundle extra, View toggledView) { }

        @Override
        public boolean getItemToggleState() {
            return false;
        }
    }

    public interface SettingItemListener {
        void onHelpClicked();

        void onPrivacyClicked();

        void onTermsClicked();

        void onLanguageClicked();

        void onNotificationClicked(View toggledView, boolean isOn);
    }

    public interface ISettingSelected {
        String getItemName();

        String getItemSubName();

        void onItemSelected(Bundle extra);

        void onItemToggleSelected(Bundle extra, View toggledView);

        boolean getItemToggleState();
    }

}

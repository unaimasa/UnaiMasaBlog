package com.unaimasa.unaimasablog.section.blog.callback;

import com.unaimasa.unaimasablog.entity.rest.Blogmedia;

import java.util.List;

/**
 * Created by unai.masa on 13/04/2016.
 */
public interface BlogCallbacks {

    void onGetPassesReady(List<Blogmedia> passes);

}

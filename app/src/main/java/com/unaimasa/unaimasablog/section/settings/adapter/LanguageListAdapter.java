package com.unaimasa.unaimasablog.section.settings.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.unaimasa.unaimasablog.Constants;
import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.section.settings.helper.LanguageHandlerHelper;
import com.unaimasa.unaimasablog.section.settings.listener.LanguageListListener;
import com.unaimasa.unaimasablog.util.SharedPreferencesUtil;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by unai.masa on 17/06/2016.
 */
public class LanguageListAdapter extends ArrayAdapter<String> {

    private LayoutInflater mInflater;
    private String[] mLanguages;

    private LanguageListListener mListener;

    public LanguageListAdapter(Context context, int resource, int tv_language_name, String[] languages, LanguageListListener listener) {
        super(context, resource, tv_language_name, languages);
        mInflater = LayoutInflater.from(getContext());
        mLanguages = languages;
        mListener = listener;
    }

    @LayoutRes
    protected int getItemResId() {
        return R.layout.item_language_list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = mInflater.inflate(getItemResId(), parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        updateItemUi(holder, position);
        return convertView;
    }

    private void updateItemUi(final ViewHolder holder, final int position) {
        final String language = mLanguages[position];
        final int language_id = LanguageHandlerHelper.getLocalizedLanguageId(SharedPreferencesUtil.getInstance().getString(Constants.SharedKeys.LANGUAGE_SELECTED_CODE, Locale.getDefault().getLanguage()));
        if(language_id == position){
            holder.containerItem.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryLight));
        }else{
            holder.containerItem.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorWhite));
        }
        holder.languageName.setText(language);
        holder.containerItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String localizedLanguageKey = LanguageHandlerHelper.getLocalizedLanguageKey(position);
                SharedPreferencesUtil.getInstance().putString(Constants.SharedKeys.LANGUAGE_SELECTED_CODE, localizedLanguageKey);
                notifyDataSetChanged();
                mListener.languageSelected(localizedLanguageKey);
            }
        });
    }

    class ViewHolder {
        @Bind(R.id.container_li_setting)
        ViewGroup containerItem;

        @Bind(R.id.tv_li_language_name)
        TextView languageName;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}

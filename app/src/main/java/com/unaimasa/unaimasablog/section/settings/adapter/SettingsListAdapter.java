package com.unaimasa.unaimasablog.section.settings.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.section.settings.fragment.SettingsFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by unai.masa on 18/04/2016.
 */
public class SettingsListAdapter extends ArrayAdapter<SettingsFragment.Setting> {

    private LayoutInflater mInflater;

    public SettingsListAdapter(Context context) {
        super(context, 0);
        mInflater = LayoutInflater.from(getContext());
    }

    // robolectric cannot work with SwitchCompat, therefore for test we'll return another xml
    @LayoutRes
    protected int getItemResId() {
        return R.layout.item_settings_list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = mInflater.inflate(getItemResId(), parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        updateItemUi(holder, position);

        return convertView;
    }

    private void updateItemUi(final ViewHolder holder, int position) {
        final SettingsFragment.Setting setting = getItem(position);
        switch (setting.getType()) {
            case ITEM:
                holder.containerInner.setVisibility(View.GONE);
                holder.name.setText(setting.getItemName());
                holder.containerItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setting.onItemSelected(null);
                    }
                });
                break;
            case BUTTON:
                holder.name.setText(setting.getItemName());
                holder.containerInner.setVisibility(View.VISIBLE);
                holder.btnInner.setVisibility(View.VISIBLE);
                holder.btnInner.setText(setting.getItemSubName());
                holder.toggleInner.setVisibility(View.GONE);
                holder.btnInner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setting.onItemSelected(null);
                    }
                });
                break;
            case TOGGLE:
                holder.name.setText(setting.getItemName());
                holder.containerInner.setVisibility(View.VISIBLE);
                holder.btnInner.setVisibility(View.GONE);
                holder.toggleInner.setChecked(setting.getItemToggleState());
                holder.toggleInner.setVisibility(View.VISIBLE);
                holder.toggleInner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle extra = new Bundle();
                        extra.putBoolean(SettingsFragment.EXTRA_SETTING_ITEM_IS_TOGGLED,
                                holder.toggleInner.isChecked());
                        setting.onItemSelected(extra);
                        setting.onItemToggleSelected(extra, v);
                    }
                });
                break;
        }

    }

    class ViewHolder {
        @Bind(R.id.container_li_setting)
        ViewGroup containerItem;
        @Bind(R.id.tv_li_setting_name)
        TextView name;
        @Bind(R.id.container_li_setting_inner)
        ViewGroup containerInner;
        @Bind(R.id.btn_li_setting_inner)
        Button btnInner;
        @Bind(R.id.btn_li_toggle_setting)
        CompoundButton toggleInner;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}

package com.unaimasa.unaimasablog.section.blog.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.unaimasa.unaimasablog.Constants;
import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.entity.rest.Blogmedia;
import com.unaimasa.unaimasablog.util.SharedPreferencesUtil;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by unai.masa on 02/06/2016.
 */
public class BlogPostSpinnerAdapter extends ArrayAdapter<Blogmedia> {

    ArrayList<Blogmedia> mBlogmediaPublishedCollection;

    public BlogPostSpinnerAdapter(Context context, int textViewResourceId, int tv_blog_title_spinner, ArrayList<Blogmedia> blogmediaPublishedCollection) {
        super(context, textViewResourceId, tv_blog_title_spinner, blogmediaPublishedCollection);
        this.mBlogmediaPublishedCollection = blogmediaPublishedCollection;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.item_blog_post_list, null);
        }

        ViewHolder holder = new ViewHolder(v);
        v.setTag(holder);

        Blogmedia blogmedia = mBlogmediaPublishedCollection.get(position);
        String title = blogmedia.getTitle();
        String date = blogmedia.getDate();

        holder.mBlogTitle.setText(Html.fromHtml(title));
        holder.mBlogDate.setText(Html.fromHtml(date));
        holder.mBlogDropDownIcon.setVisibility(View.VISIBLE);
        return v;
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.item_blog_post_list, null);
        }

        ViewHolder holder = new ViewHolder(v);
        v.setTag(holder);

        Blogmedia blogmedia = mBlogmediaPublishedCollection.get(position);
        int currentPosition = SharedPreferencesUtil.getInstance().getInt(Constants.SharedKeys.BLOG_POSITION, 0);
        String title = blogmedia.getTitle();
        String date = blogmedia.getDate();

        if(currentPosition == position){
            holder.mItemContainer.setBackgroundColor(ContextCompat.getColor(this.getContext(), R.color.colorPrimaryLight));
        }else{
            holder.mItemContainer.setBackgroundColor(ContextCompat.getColor(this.getContext(), R.color.colorWhite));
        }

        holder.mBlogTitle.setText(Html.fromHtml(title));
        holder.mBlogDate.setText(Html.fromHtml(date));
        holder.mBlogDropDownIcon.setVisibility(View.GONE);
        return v;
    }

    static class ViewHolder {
        @Bind(R.id.item_container)
        RelativeLayout mItemContainer;

        @Bind(R.id.tv_blog_title_spinner)
        TextView mBlogTitle;

        @Bind(R.id.tv_blog_date_spinner)
        TextView mBlogDate;

        @Bind(R.id.iv_drop_down_icon_spinner)
        ImageView mBlogDropDownIcon;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}

package com.unaimasa.unaimasablog.section.splash;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.UnaiMasaBlogApp;
import com.unaimasa.unaimasablog.base.ui.activity.BaseActivity;
import com.unaimasa.unaimasablog.section.core.activity.MainActivity;
import com.unaimasa.unaimasablog.section.splash.background.CircleProgressViewOperation;

import at.grabner.circleprogress.AnimationState;
import at.grabner.circleprogress.AnimationStateChangedListener;
import at.grabner.circleprogress.CircleProgressView;
import at.grabner.circleprogress.TextMode;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by unai.masa on 19/05/2016.
 */
public class SplashActivity extends BaseActivity {

    // ===== Progress View ===== //
    @Bind(R.id.circleView)
    protected CircleProgressView circleProgressView;
    private float mProgressValue = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        circleProgressView.setSeekModeEnabled(false);
        circleProgressView.setValue(0);
        circleProgressView.spin();

        circleProgressView.setOnProgressChangedListener(new CircleProgressView.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(float value) {
                mProgressValue = mProgressValue + value;
            }
        });

        circleProgressView.setBackgroundResource(R.mipmap.ic_launcher);
        circleProgressView.setOnAnimationStateChangedListener(
            new AnimationStateChangedListener() {
                @Override
                public void onAnimationStateChanged(AnimationState _animationState) {
                    switch (_animationState) {
                        case IDLE:
                            if(mProgressValue == 100){
                                Intent mainActivityIntent = new Intent(UnaiMasaBlogApp.getInstance(), MainActivity.class);
                                startActivity(mainActivityIntent);
                                finish();
                            }
                            break;
                        case ANIMATING:
                            break;
                        case START_ANIMATING_AFTER_SPINNING:
                            break;
                        case SPINNING:
                            circleProgressView.setTextMode(TextMode.TEXT); // show text while spinning
                            circleProgressView.setUnitVisible(false);
                            break;
                        case END_SPINNING:
                            break;
                        case END_SPINNING_START_ANIMATING:
                            break;

                    }
                }
            }
        );
        new CircleProgressViewOperation(this, circleProgressView).execute();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ImageLoader.getInstance().clearMemoryCache();
    }

    @Override
    public void onBackPressed() { }
}

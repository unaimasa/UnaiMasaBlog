package com.unaimasa.unaimasablog.section.map.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.unaimasa.unaimasablog.Constants;
import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.util.SharedPreferencesUtil;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by unai.masa on 04/05/2016.
 */
public class MapTypeSpinnerAdapter extends ArrayAdapter {

    String[] types;

    public MapTypeSpinnerAdapter(Context context, int textViewResourceId, int tv_map_type, String[] types) {
        super(context, textViewResourceId, tv_map_type, types);
        this.types = types;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.item_map_type_list, null);
        }

        ViewHolder holder = new ViewHolder(v);
        v.setTag(holder);

        String type = types[position];
        holder.mMapType.setText(type);
        holder.mMapDropDownIcon.setVisibility(View.VISIBLE);
        return v;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.item_map_type_list, null);
        }

        ViewHolder holder = new ViewHolder(v);
        v.setTag(holder);

        String type = types[position];
        int currentType = SharedPreferencesUtil.getInstance().getInt(Constants.SharedKeys.MAP_TYPE, 0);

        if(currentType == position){
            holder.mItemContainer.setBackgroundColor(ContextCompat.getColor(this.getContext(), R.color.colorPrimaryLight));
        }else{
            holder.mItemContainer.setBackgroundColor(ContextCompat.getColor(this.getContext(), R.color.colorWhite));
        }

        holder.mMapType.setText(type);
        holder.mMapDropDownIcon.setVisibility(View.GONE);
        return v;
    }

    public void setMapTypeSpinnerItemInformation(String type, ViewHolder holder) {
        holder.mMapType.setText(type);
    }

    static class ViewHolder {
        @Bind(R.id.item_container)
        RelativeLayout mItemContainer;

        @Bind(R.id.tv_map_type)
        TextView mMapType;

        @Bind(R.id.iv_drop_down_icon_spinner)
        ImageView mMapDropDownIcon;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}

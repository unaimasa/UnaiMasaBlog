package com.unaimasa.unaimasablog.section.settings.fragment;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.webkit.WebView;

import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.base.ui.fragment.BaseFragment;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by unai.masa on 18/04/2016.
 */
public abstract class BaseHtmlSettingsFragment extends BaseFragment {

    @Bind(R.id.wv_html)
    protected WebView mWvHtml;

    protected Toolbar mToolbar;
    protected Menu mOptionsMenu;

    @Override
    protected void afterInject(View rootView, Bundle savedInstanceState) {

        // Put Toolbar Title
        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mToolbar.setTitle(getScreenTitle());

        // Get Toolbar Menu
        mOptionsMenu = mToolbar.getMenu();

        // HTML View
        mWvHtml.loadUrl(getContentUrl());
    }

    @Override
    protected int getCustomLayoutId() {
        return R.layout.fragment_settings_html_base;
    }

    protected abstract String getScreenTitle();

    protected abstract String getContentUrl();

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        setHomeToolbar(mOptionsMenu);
    }

}

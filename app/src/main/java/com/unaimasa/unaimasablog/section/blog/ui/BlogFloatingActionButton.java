package com.unaimasa.unaimasablog.section.blog.ui;

import android.content.Context;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Spinner;

import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.UnaiMasaBlogApp;

/**
 * Created by unai.masa on 03/06/2016.
 * Class that handles blog's Floating Action Button
 */
public class BlogFloatingActionButton extends FloatingActionButton {

    private Spinner mSpinner;

    public BlogFloatingActionButton(Context context) {
        super(context);
    }

    public BlogFloatingActionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BlogFloatingActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Method that sets the spinner
     * @param spinner
     */
    public void setSpinner(Spinner spinner) {
        this.mSpinner = spinner;
    }

    /**
     * Method that shows or hides the Spinner
     */
    public void setBlogPostSpinnerVisivility(){
        if(mSpinner.getVisibility() == View.GONE) {
            mSpinner.setVisibility(View.VISIBLE);
        }else{
            mSpinner.setVisibility(View.GONE);
        }
    }

}

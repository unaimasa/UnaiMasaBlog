package com.unaimasa.unaimasablog.section.settings.helper;

import com.unaimasa.unaimasablog.Constants;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by unai.masa on 17/06/2016.
 */
public class LanguageHandlerHelper {

    private static final Map<String, Integer> localeLanguageCodes = new HashMap<>();

    static {
        localeLanguageCodes.put(Constants.LanguageCodes.SPANISH_LANGUAGE_CODE, 0);
        localeLanguageCodes.put(Constants.LanguageCodes.ENGLISH_LANGUAGE_CODE, 1);
    }

    public static int getLocalizedLanguageId(String languageCode) {
        int localeLanguageCodeId;
        try {
            localeLanguageCodeId = localeLanguageCodes.get(languageCode);
        } catch(Exception e) {
            localeLanguageCodeId = localeLanguageCodes.get(Constants.LanguageCodes.ENGLISH_LANGUAGE_CODE);
        }
        return localeLanguageCodeId;
    }

    public static String getLocalizedLanguageKey(int languageId) {
        String localeLanguageCodeKey = Constants.LanguageCodes.ENGLISH_LANGUAGE_CODE;
        for(Map.Entry<String, Integer> entry: localeLanguageCodes.entrySet()) {
            if (entry.getValue().intValue() == languageId) {
                localeLanguageCodeKey = entry.getKey();
            }
        }
        return localeLanguageCodeKey;
    }

}

package com.unaimasa.unaimasablog.section.portfolio;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.unaimasa.unaimasablog.Constants;
import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.base.ui.fragment.BaseFragment;
import com.unaimasa.unaimasablog.entity.Project;
import com.unaimasa.unaimasablog.section.portfolio.adapter.ProjectCardViewAdapter;
import com.unaimasa.unaimasablog.util.media.UIL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import butterknife.Bind;

/**
 * Created by unai.masa on 15/04/2016.
 */
public class PortfolioFragment extends BaseFragment {

    private static final Logger sLogger = LoggerFactory.getLogger(PortfolioFragment.class);

    // ===== Portfolio Elements ===== //
    @Bind(R.id.rv_projects_card_view)
    protected RecyclerView mRVCardView;

    private LinearLayoutManager mLinearLayoutManager;
    private StaggeredGridLayoutManager mStaggeredGridLayoutManager;
    private ProjectCardViewAdapter mProjectCardViewAdapter;
    private ArrayList<Project> mProjects;

    @Bind(R.id.tv_portfolio_introduction)
    protected TextView mTVPortfolioIntroduction;

    @Bind(R.id.iv_portfolio_image_1)
    protected ImageView mIVPortfolio1;

    @Bind(R.id.tv_portfolio_title_1)
    protected TextView mTVPortfolioTitle1;

    @Bind(R.id.tv_portfolio_text_1)
    protected TextView mTVPortfolioText1;

    @Bind(R.id.iv_portfolio_image_2)
    protected ImageView mIVPortfolio2;

    @Bind(R.id.tv_portfolio_title_2)
    protected TextView mTVPortfolioTitle2;

    @Bind(R.id.tv_portfolio_text_2)
    protected TextView mTVPortfolioText2;

    @Bind(R.id.iv_portfolio_image_3)
    protected ImageView mIVPortfolio3;

    @Bind(R.id.tv_portfolio_title_3)
    protected TextView mTVPortfolioTitle3;

    @Bind(R.id.tv_portfolio_text_3)
    protected TextView mTVPortfolioText3;

    protected Toolbar mToolbar;
    protected Menu mOptionsMenu;

    @Override
    protected void afterInject(View rootView, Bundle savedInstanceState) {

        // Put Toolbar Title
        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.drawer_portfolio_text);

        // Get Toolbar Menu
        mOptionsMenu = mToolbar.getMenu();

        // Hide Home Button
        setHomeToolbar(mOptionsMenu);

        // Create Array List of Projects
        mProjects = new ArrayList<Project>();
        mProjects = getProjects();

        // Set Linear Layout for Recycler View
        mRVCardView.setHasFixedSize(true);
        /*
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRVCardView.setLayoutManager(mLinearLayoutManager);
        */
        mStaggeredGridLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mRVCardView.setLayoutManager(mStaggeredGridLayoutManager);

        mProjectCardViewAdapter = new ProjectCardViewAdapter(mProjects);
        mRVCardView.setAdapter(mProjectCardViewAdapter);

        // Load Portfolio Images
        UIL.bind(Constants.AppPhotos.PORTFOLIO_PHOTO_1, mIVPortfolio1).setLoadingListener(mImageLoadingListener).load();
        UIL.bind(Constants.AppPhotos.PORTFOLIO_PHOTO_2, mIVPortfolio2).setLoadingListener(mImageLoadingListener).load();
        UIL.bind(Constants.AppPhotos.PORTFOLIO_PHOTO_3, mIVPortfolio3).setLoadingListener(mImageLoadingListener).load();

    }

    @Override
    protected int getCustomLayoutId() { return R.layout.fragment_portfolio; }

    // ===== Universal Image Loader Methods ===== //
    private ImageLoadingListener mImageLoadingListener = new SimpleImageLoadingListener() {
        @Override
        public void onLoadingStarted(String imageUri, View view) { }

        @Override
        public void onLoadingFailed(String imageUri, View view, FailReason failReason) { }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) { }
    };

    // ===== Create Array List of Project ===== //
    public ArrayList<Project> getProjects(){
        mProjects.clear();

        Project chickenRain = new Project("Chicken Rain", Constants.AppPhotos.PORTFOLIO_PHOTO_PROJECT_CHICKEN_RAIN, "com.engine_one.engine", Project.ProjectPlatform.ANDROID);
        Project saveANote = new Project("Save A Note", Constants.AppPhotos.PORTFOLIO_PHOTO_PROJECT_SAVE_A_NOTE, "com.unai.saveanote", Project.ProjectPlatform.ANDROID);
        Project pixelUDPChat = new Project("Pixel UDP Chat", Constants.AppPhotos.PORTFOLIO_PHOTO_PROJECT_PIXEL_UDP_CHAT, "com.unai.pixeludpchat", Project.ProjectPlatform.ANDROID);
        Project ble = new Project("BLE", Constants.AppPhotos.PORTFOLIO_PHOTO_PROJECT_BLE, "", Project.ProjectPlatform.ANDROID);
        Project lightsManagement = new Project("Lights Management", Constants.AppPhotos.PORTFOLIO_PHOTO_PROJECT_LIGHTS_MANAGEMENT, "", Project.ProjectPlatform.MULTIPLATFORM);
        Project shopsManagement = new Project("Shops Management", Constants.AppPhotos.PORTFOLIO_PHOTO_PROJECT_SHOPS_MANAGEMENT, "", Project.ProjectPlatform.MULTIPLATFORM);
        Project vehiclesManagement = new Project("Vehicles Management", Constants.AppPhotos.PORTFOLIO_PHOTO_PROJECT_VEHICLES_MANAGEMENT, "", Project.ProjectPlatform.MULTIPLATFORM);

        mProjects.add(chickenRain);
        mProjects.add(saveANote);
        mProjects.add(pixelUDPChat);
        mProjects.add(ble);
        mProjects.add(lightsManagement);
        mProjects.add(shopsManagement);
        mProjects.add(vehiclesManagement);

        return mProjects;
    }

}

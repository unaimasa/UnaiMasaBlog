package com.unaimasa.unaimasablog.rest;

import com.unaimasa.unaimasablog.Constants;
import com.unaimasa.unaimasablog.entity.rest.Blogmedia;
import com.unaimasa.unaimasablog.entity.rest.BlogmediaPublishedCollection;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;


/**
 * Created by unai.masa on 05/04/2016.
 * Main interface which describe all REST API
 */
public interface IWebApi {

    /*
     * interface for Post new Blogmedia
     */
    @FormUrlEncoded
    @POST("/blogmedia")
    Call<BlogmediaPublishedCollection> putPublishedBlogPost(@Field("title") String title,
                                                      @Field("date") String date,
                                                      @Field("video_url") String video_url,
                                                      @Field("first_paragraph") String first_paragraph,
                                                      @Field("subtitle") String subtitle,
                                                      @Field("instagram_image_url") String instagram_image_url,
                                                      @Field("image_url") String image_url,
                                                      @Field("second_paragraph") String second_paragraph,
                                                      @Field("thanks_paragraph") String thanks_paragraph);

    /**
     * interface for Get Blogmedia Published
     */
    @GET("/{path}")
    Call<BlogmediaPublishedCollection> getBlogmediaPublished(@Header(Constants.Header.ETAG) String etag, @Path(encoded = true, value = "path") String path);

    /**
     * interface for Get Blogmedia Published by Id
     */
    @GET("/blogmedia/{id}")
    Call<Blogmedia> getBlogmediaPublishedById(@Path(encoded = false, value = "id") String blogId);

    /*
     * interface for Update Blogmedia
     */
    @FormUrlEncoded
    @POST("/blogmedia/{id}")
    Call<Blogmedia> updatePublishedBlogPost(@Path(encoded = false, value = "id") String blogId,
                                      @Field("id") String id,
                                      @Field("title") String title,
                                      @Field("date") String date,
                                      @Field("video_url") String video_url,
                                      @Field("first_paragraph") String first_paragraph,
                                      @Field("subtitle") String subtitle,
                                      @Field("instagram_image_url") String instagram_image_url,
                                      @Field("image_url") String image_url,
                                      @Field("second_paragraph") String second_paragraph,
                                      @Field("thanks_paragraph") String thanks_paragraph);

}

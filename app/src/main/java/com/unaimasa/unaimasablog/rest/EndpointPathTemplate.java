package com.unaimasa.unaimasablog.rest;

/**
 * Created by unai.masa on 05/04/2016.
 */
public enum EndpointPathTemplate {

    // ===== Blogmedia ===== //
    // POST Blogmedia
    POST_BLOGMEDIA("api/blogmedia"),
    // GET Blogmedia Published
    GET_BLOGMEDIA_PUBLISHED("api/blogmedia/published"),
    // GET Blogmedia by Id
    GET_BLOGMEDIA_BY_ID("api/blogmedia/(.*)"),
    // EDIT Blogmedia by Id
    EDIT_BLOGMEDIA_BY_ID("api/blogmedia/(.*)");

    private final String mPathTemplate;

    EndpointPathTemplate(String pathTemplate) {
        mPathTemplate = pathTemplate;
    }

    public String getPathTemplate() {
        return mPathTemplate;
    }

}

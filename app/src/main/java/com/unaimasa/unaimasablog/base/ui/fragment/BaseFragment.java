package com.unaimasa.unaimasablog.base.ui.fragment;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.ViewStubCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.unaimasa.unaimasablog.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by unai.masa on 13/04/2016.
 */
public abstract class BaseFragment extends Fragment {

    @Nullable
    @Bind(R.id.progress_full_screen)
    protected View mProgressFullScreen;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView;
        int customLayoutId = getCustomLayoutId();
        if (customLayoutId > 0) {
            rootView = (ViewGroup) inflater.inflate(customLayoutId, container, false);
        } else {
            rootView = (ViewGroup) inflater.inflate(R.layout.fragment_base, container, false);
        }
        ButterKnife.bind(this, rootView);
        afterInject(rootView, savedInstanceState);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    /**
     * @return null if uses custom layout, otherwise not-null View
     */
    public View getProgressFullScreen() {
        return mProgressFullScreen;
    }

    /**
     * shows progress in full screen, including toolbar
     */
    protected void showProgressFullScreen() {
        mProgressFullScreen.setVisibility(View.VISIBLE);
    }
    /**
     * hides progress full screen, including toolbar
     */
    
    protected void hideProgressFullScreen() {
        mProgressFullScreen.setVisibility(View.GONE);
    }

    /**
     * @return if bigger than 0 then fragment will have this layout as resources,
     * otherwise - default layout uses with toolbar and content layouts
     */
    @LayoutRes
    protected int getCustomLayoutId() {
        return 0;
    }

    protected abstract void afterInject(View rootView, Bundle savedInstanceState);

    // ===== UI Methods ===== //
    public void setHomeToolbar(Menu optionsMenu){
        showCloseButton(optionsMenu, false);
        showHomeButton(optionsMenu, true);
    }

    public void hideHomeToolbar(Menu optionsMenu){
        showCloseButton(optionsMenu, false);
        showHomeButton(optionsMenu, false);
    }

    public void setSettingsToolbar(Menu optionsMenu){
        showCloseButton(optionsMenu, true);
        showHomeButton(optionsMenu, false);
    }


    public void showHomeButton(Menu optionsMenu, boolean show) {
        if(optionsMenu != null) {
            optionsMenu.setGroupVisible(R.id.main_menu_home_group, show);
        }
    }

    public void showCloseButton(Menu optionsMenu, boolean show) {
        if(optionsMenu != null) {
            optionsMenu.setGroupVisible(R.id.main_menu_close_group, show);
        }
    }

    public void refreshFragment() {
        Fragment currentFragment = this;
        FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
        fragTransaction.detach(currentFragment);
        fragTransaction.attach(currentFragment);
        fragTransaction.commit();
    }

}

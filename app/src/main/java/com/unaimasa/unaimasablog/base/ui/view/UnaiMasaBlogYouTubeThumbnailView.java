package com.unaimasa.unaimasablog.base.ui.view;

import android.content.Context;
import android.view.View;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.unaimasa.unaimasablog.BuildConfig;
import com.unaimasa.unaimasablog.base.listener.UnaiMasaBlogScrollViewListener;
import com.unaimasa.unaimasablog.base.listener.UnaiMasaBlogYouTubeThumbnailViewListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by unai.masa on 06/06/2016.
 */
public class UnaiMasaBlogYouTubeThumbnailView implements YouTubeThumbnailView.OnInitializedListener {

    private static final Logger sLogger = LoggerFactory.getLogger(UnaiMasaBlogYouTubeThumbnailView.class);

    private UnaiMasaBlogYouTubeThumbnailViewListener mUnaiMasaBlogYouTubeThumbnailViewListener = null;

    private Context mContext;

    private YouTubeThumbnailView mYouTubeThumbnailView;
    private YouTubeThumbnailLoader mYouTubeThumbnailLoader;

    public UnaiMasaBlogYouTubeThumbnailView(Context context, YouTubeThumbnailView youTubeThumbnailView) {
        this.mContext = context;
        this.mYouTubeThumbnailView = youTubeThumbnailView;
        initializeYouTubeThumbnail();
    }

    public void setUnaiMasaBlogYouTubeThumbnailViewListener(UnaiMasaBlogYouTubeThumbnailViewListener unaiMasaBlogYouTubeThumbnailViewListener) {
        this.mUnaiMasaBlogYouTubeThumbnailViewListener = unaiMasaBlogYouTubeThumbnailViewListener;
    }

    // Method to Initialize YouTube Thumbnail and set listener
    private void initializeYouTubeThumbnail() {
        mYouTubeThumbnailView.initialize(BuildConfig.YOUTUBE_API_KEY, this);
        mYouTubeThumbnailView.setOnClickListener(new YoutubeThumbnailViewOnClickListener());
    }

    public void releaseYouTubeThumbnailLoader(){
        if(mYouTubeThumbnailLoader != null) {
            mYouTubeThumbnailLoader.release();
        }
    }

    // Listener to onClick YouTube Thumbnail
    public class YoutubeThumbnailViewOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            mUnaiMasaBlogYouTubeThumbnailViewListener.onThumbnailClick();
        }
    }

    public void setYouTubeThumbnailLoaderVideo(String videoId) {
        if(mYouTubeThumbnailLoader != null) {
            mYouTubeThumbnailLoader.setVideo(videoId);
        }
    }



    // ===== Youtube Methods ===== //
    @Override
    public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {
        sLogger.info("YouTubeThumbnailView.onInitializationSuccess()");
        mYouTubeThumbnailLoader = youTubeThumbnailLoader;
        mYouTubeThumbnailLoader.setOnThumbnailLoadedListener(new ThumbnailLoadedListener());
        // Callback to set Image on Success
        mUnaiMasaBlogYouTubeThumbnailViewListener.onInitializationSuccess();

    }

    @Override
    public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {
        sLogger.info("YouTubeThumbnailView.onInitializationFailure()");
        // Callback to set Image on Failure
        mUnaiMasaBlogYouTubeThumbnailViewListener.onInitializationFailure();
    }

    // ===== YouTube ThumbnailLoaderListener ===== //
    private final class ThumbnailLoadedListener implements YouTubeThumbnailLoader.OnThumbnailLoadedListener {

        @Override
        public void onThumbnailError(YouTubeThumbnailView arg0, YouTubeThumbnailLoader.ErrorReason arg1) {
            sLogger.info("ThumbnailLoadedListener.onThumbnailError()");
        }

        @Override
        public void onThumbnailLoaded(YouTubeThumbnailView arg0, String arg1) {
            sLogger.info("ThumbnailLoadedListener.onThumbnailLoaded()");
        }
    }
}

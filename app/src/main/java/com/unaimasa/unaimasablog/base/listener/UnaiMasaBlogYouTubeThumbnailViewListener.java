package com.unaimasa.unaimasablog.base.listener;

/**
 * Created by unai.masa on 06/06/2016.
 */
public interface UnaiMasaBlogYouTubeThumbnailViewListener {

    void onInitializationSuccess();

    void onInitializationFailure();

    void onThumbnailClick();
}

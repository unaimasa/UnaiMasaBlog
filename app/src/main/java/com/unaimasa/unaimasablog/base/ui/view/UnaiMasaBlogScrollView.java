package com.unaimasa.unaimasablog.base.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

import com.unaimasa.unaimasablog.base.listener.UnaiMasaBlogScrollViewListener;

/**
 * Created by unai.masa on 03/06/2016.
 */
public class UnaiMasaBlogScrollView extends ScrollView {

    private UnaiMasaBlogScrollViewListener mScrollViewListener = null;

    public UnaiMasaBlogScrollView(Context context) {
        super(context);
    }

    public UnaiMasaBlogScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public UnaiMasaBlogScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setScrollViewListener(UnaiMasaBlogScrollViewListener scrollViewListener) {
        this.mScrollViewListener = scrollViewListener;
    }

    @Override
    protected void onScrollChanged(int x, int y, int oldx, int oldy) {
        super.onScrollChanged(x, y, oldx, oldy);
        if(mScrollViewListener != null) {
            mScrollViewListener.onScrollChanged(this, x, y, oldx, oldy);
        }
    }

}

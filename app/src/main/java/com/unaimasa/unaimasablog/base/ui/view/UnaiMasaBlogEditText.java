package com.unaimasa.unaimasablog.base.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.base.ui.fonts.FontsManagerHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by unai.masa on 29/04/2016.
 * EditText with custom font applied
 */
public class UnaiMasaBlogEditText extends AppCompatEditText {

    private static final Logger logger = LoggerFactory.getLogger(UnaiMasaBlogEditText.class);

    public UnaiMasaBlogEditText(Context context) {
        super(context);
    }

    public UnaiMasaBlogEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        processCustomAttributes(attrs);
    }

    public UnaiMasaBlogEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        processCustomAttributes(attrs);
    }


    private void processCustomAttributes(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.UnaiMasaBlogEditText);
        int customFontFilenameId = a.getInt(R.styleable.UnaiMasaBlogEditText_customFont, -1);
        a.recycle();
        FontsManagerHelper.FontName fontName =
                FontsManagerHelper.FontName.getFontFilenameFromId(customFontFilenameId);
        if (fontName != FontsManagerHelper.FontName.NONE) {
            setCustomFont(fontName);
        }
    }

    public boolean setCustomFont(FontsManagerHelper.FontName fontName) {
        Typeface tf = null;
        try {
            tf = FontsManagerHelper.getTypeFace(getContext(), fontName.getFontFilename());
            setTypeface(tf);
        } catch (Exception e) {
            logger.error("Could not get typeface", e);
            return false;
        }
        return true;
    }

}

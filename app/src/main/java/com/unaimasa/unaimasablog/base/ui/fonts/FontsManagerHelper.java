package com.unaimasa.unaimasablog.base.ui.fonts;

import android.content.Context;
import android.graphics.Typeface;

import java.util.WeakHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by unai.masa on 08/04/2016.
 */
public class FontsManagerHelper {

    public static final String TYPEFACE_FOLDER = "fonts";

    private static final Lock lockObject = new ReentrantLock();

    private static WeakHashMap<String, Typeface> sTypeFaces = new WeakHashMap<String, Typeface>();

    public static Typeface getTypeFace(Context context, String fileName) {
        Typeface tempTypeface = null;
        lockObject.lock();
        try {
            tempTypeface = sTypeFaces.get(fileName);
            if (tempTypeface == null) {
                String fontPath = TYPEFACE_FOLDER + "/" + fileName;
                tempTypeface = Typeface.createFromAsset(context.getAssets(), fontPath);
                sTypeFaces.put(fileName, tempTypeface);
            }
        } finally {
            lockObject.unlock();
        }
        return tempTypeface;
    }

    /**
     * if any font is added be sure change it links to right value in res/values/attrs.xml
     * */
    public enum FontName {
        NONE(""),

        ROBOTO_BLACK("Roboto-Black.ttf"),
        ROBOTO_BLACK_ITALIC("Roboto-BlackItalic.ttf"),
        ROBOTO_BOLD("Roboto-Bold.ttf"),
        ROBOTO_BOLD_ITALIC("Roboto-BoldItalic.ttf"),
        ROBOTO_ITALIC("Roboto-Italic.ttf"),
        ROBOTO_LIGHT("Roboto-Light.ttf"),
        ROBOTO_LIGHT_ITALIC("Roboto-LightItalic.ttf"),
        ROBOTO_MEDIUM("Roboto-Medium.ttf"),
        ROBOTO_MEDIUM_ITALIC("Roboto-MediumItalic.ttf"),
        ROBOTO_REGULAR("Roboto-Regular.ttf"),
        ROBOTO_THIN("Roboto-Thin.ttf"),
        ROBOTO_THIN_ITALIC("Roboto-ThinItalic.ttf"),
        ROBOTO_CONDENSED_BOLD("RobotoCondensed-Bold.ttf"),
        ROBOTO_CONDENSED_BOLD_ITALIC("RobotoCondensed-BoldItalic.ttf"),
        ROBOTO_CONDENSED_ITALIC("RobotoCondensed-Italic.ttf"),
        ROBOTO_CONDENSED_LIGHT("RobotoCondensed-Light.ttf"),
        ROBOTO_CONDENSED_LIGHT_ITALIC("RobotoCondensed-LightItalic.ttf"),
        ROBOTO_CONDENSED_REGULAR("RobotoCondensed-Regular.ttf");

        private String mFontFilename;

        FontName(String fontFilename) {
            this.mFontFilename = fontFilename;
        }

        public String getFontFilename() {
            return mFontFilename;
        }

        /**
         * get font filename from id according to xml attribute enum value
         *
         * */
        public static FontName getFontFilenameFromId(int id) {
            switch (id) {
                // ROBOTO
                case 1:
                    return ROBOTO_BLACK;
                case 2:
                    return ROBOTO_BLACK_ITALIC;
                case 3:
                    return ROBOTO_BOLD;
                case 4:
                    return ROBOTO_BOLD_ITALIC;
                case 5:
                    return ROBOTO_ITALIC;
                case 6:
                    return ROBOTO_LIGHT;
                case 7:
                    return ROBOTO_LIGHT_ITALIC;
                case 8:
                    return ROBOTO_MEDIUM;
                case 9:
                    return ROBOTO_MEDIUM_ITALIC;
                case 10:
                    return ROBOTO_REGULAR;
                case 11:
                    return ROBOTO_THIN;
                case 12:
                    return ROBOTO_THIN_ITALIC;
                case 13:
                    return ROBOTO_CONDENSED_BOLD;
                case 14:
                    return ROBOTO_CONDENSED_BOLD_ITALIC;
                case 15:
                    return ROBOTO_CONDENSED_ITALIC;
                case 16:
                    return ROBOTO_CONDENSED_LIGHT;
                case 17:
                    return ROBOTO_CONDENSED_LIGHT_ITALIC;
                case 18:
                    return ROBOTO_CONDENSED_REGULAR;

                default:
                    return NONE;
            }

        }
    }

}

package com.unaimasa.unaimasablog.base.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by unai.masa on 05/04/2016.
 * Base activity for all activities in app.
 * The place when logging, general methods should be implemented.
 * All activities has SpiceManager and can execute requests using DataManager
 */
public class BaseActivity extends AppCompatActivity {

    private boolean mResumed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume(){
        super.onResume();
        mResumed = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public boolean isActivityResumed(){
        return mResumed;
    }

}

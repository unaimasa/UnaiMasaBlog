package com.unaimasa.unaimasablog.base.ui.activity;

import android.os.Bundle;
import android.support.annotation.LayoutRes;

import butterknife.ButterKnife;

/**
 * Created by unai.masa on 08/04/2016.
 */
public abstract class ButterKnifeActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResId());
        ButterKnife.bind(this);
        afterInject(savedInstanceState);
    }

    protected void afterInject(Bundle savedInstanceState) {

    }

    @LayoutRes
    protected abstract int getResId();
}

package com.unaimasa.unaimasablog.base;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.unaimasa.unaimasablog.BuildConfig;
import com.unaimasa.unaimasablog.rest.EndpointPathTemplate;
import com.unaimasa.unaimasablog.util.StringUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by unai.masa on 05/04/2016.
 * This class represents any request to server.
 */
public class UriResolver {

    private static final Logger logger = LoggerFactory.getLogger(UriResolver.class);

    private final String mPath;
    private EndpointPathTemplate mEndpointId;
    private boolean force = false;

    private boolean mLoadOnlyFromDB = false;

    /**
     * create path with parameters, like 'blogmedia/(.*)' and args as values instead of '(.*)'
     */
    public UriResolver(EndpointPathTemplate endpointId, String... args) {
        this.mPath = StringUtil.format(endpointId.getPathTemplate(), args);
        mEndpointId = endpointId;
    }

    /**
     * not parametrized path, like '/blogmedia/123'
     */
    public UriResolver(String path) {
        this.mPath = path;
    }

    public String getRelPath() {
        return mPath;
    }

    /**
     * @return full url for request
     */
    public String getFullUrl() {
        return BuildConfig.API_ENDPOINT + getRelPath();
    }

    public String getLastSegmentUriData(Uri fullUri) {
        return fullUri == null ? null : fullUri.getLastPathSegment();
    }

    public EndpointPathTemplate getEndpointId() { return mEndpointId; }

    @Override
    public String toString() {
        return mPath;
    }

    public String print() {
        return mPath + "\n" +
                "RelPath " + getRelPath() + "\n" +
                "FullUrl " + getFullUrl() + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UriResolver that = (UriResolver) o;

        if (!mPath.equals(that.mPath)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return mPath.hashCode();
    }

    /**
     * @return resolver for get blogmedia published
     */
    @NonNull
    public static UriResolver getBlogmediaPublishedResolver() {
        return new UriResolver(EndpointPathTemplate.GET_BLOGMEDIA_PUBLISHED);
    }

}

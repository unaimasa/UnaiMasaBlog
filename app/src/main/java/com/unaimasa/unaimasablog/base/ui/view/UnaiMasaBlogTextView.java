package com.unaimasa.unaimasablog.base.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.base.ui.fonts.FontsManagerHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by unai.masa on 29/04/2016.
 * TextView with custom font applied
 */
public class UnaiMasaBlogTextView extends AppCompatTextView{

    private static final Logger logger = LoggerFactory.getLogger(UnaiMasaBlogTextView.class);

    public UnaiMasaBlogTextView(Context context) {
        super(context);
    }

    public UnaiMasaBlogTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        processCustomAttributes(attrs);
    }

    public UnaiMasaBlogTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        processCustomAttributes(attrs);
    }

    private void processCustomAttributes(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.UnaiMasaBlogTextView);
        int customFontFilenameId = a.getInt(R.styleable.UnaiMasaBlogTextView_customFont, -1);
        a.recycle();
        FontsManagerHelper.FontName fontName =
                FontsManagerHelper.FontName.getFontFilenameFromId(customFontFilenameId);
        if (fontName != FontsManagerHelper.FontName.NONE) {
            setCustomFont(fontName);
        }
    }

    public boolean setCustomFont(FontsManagerHelper.FontName fontName) {
        Typeface tf = null;
        try {
            tf = FontsManagerHelper.getTypeFace(getContext(), fontName.getFontFilename());
            setTypeface(tf);
        } catch (Exception e) {
            logger.error("Could not get typeface", e);
            return false;
        }
        return true;
    }

}

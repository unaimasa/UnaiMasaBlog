package com.unaimasa.unaimasablog.base.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.base.ui.fonts.FontsManagerHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by unai.masa on 29/04/2016.
 * Button with custom font applied
 */
public class UnaiMasaBlogButton extends AppCompatButton {

    private static final Logger logger = LoggerFactory.getLogger(UnaiMasaBlogButton.class);

    public UnaiMasaBlogButton(Context context) {
        this(context, null);
    }

    public UnaiMasaBlogButton(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.buttonStyle);
    }

    public UnaiMasaBlogButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        processCustomAttributes(attrs, defStyle);
    }

    private void processCustomAttributes(AttributeSet attrs, int defStyle) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.UnaiMasaBlogButton, defStyle, 0);
        int customFontFilenameId = a.getInt(R.styleable.UnaiMasaBlogButton_customFont, -1);
        a.recycle();

        FontsManagerHelper.FontName fontName =
                FontsManagerHelper.FontName.getFontFilenameFromId(customFontFilenameId);
        if (fontName != FontsManagerHelper.FontName.NONE) {
            setCustomFont(fontName);
        }
    }

    public boolean setCustomFont(FontsManagerHelper.FontName fontName) {
        Typeface tf;
        try {
            tf = FontsManagerHelper.getTypeFace(getContext(), fontName.getFontFilename());
            setTypeface(tf);
        } catch (Exception e) {
            logger.error("Could not get typeface", e);
            return false;
        }
        return true;
    }
}

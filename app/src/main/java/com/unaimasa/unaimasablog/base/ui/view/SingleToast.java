package com.unaimasa.unaimasablog.base.ui.view;

import android.text.TextUtils;
import android.widget.Toast;

import com.unaimasa.unaimasablog.UnaiMasaBlogApp;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by unai.masa on 08/04/2016.
 * Do not repeat toast with the same message
 */
public class SingleToast {

    private static final Set<CharSequence> sCurrentToasts = new HashSet<>();

    private static Toast sInstance = Toast.makeText(UnaiMasaBlogApp.getInstance(), "", Toast.LENGTH_SHORT);

    public static void show(CharSequence text, int duration) {
        if (sInstance.getView() == null) {
            // not initialized somehow - use not single instance approach (e.g. in tests get view is null)
            Toast.makeText(UnaiMasaBlogApp.getInstance(), text, duration).show();
            return;
        }
        if (!sInstance.getView().isShown()) {
            sCurrentToasts.clear();
        }
        if (!TextUtils.isEmpty(text) && !sCurrentToasts.contains(text)) {
            sCurrentToasts.add(text);
            sInstance.setText(text);
            sInstance.setDuration(duration);
            sInstance.show();
        }
    }

}

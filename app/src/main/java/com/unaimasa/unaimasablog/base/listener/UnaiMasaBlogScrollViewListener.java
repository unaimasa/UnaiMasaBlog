package com.unaimasa.unaimasablog.base.listener;

import com.unaimasa.unaimasablog.base.ui.view.UnaiMasaBlogScrollView;

/**
 * Created by unai.masa on 03/06/2016.
 */
public interface UnaiMasaBlogScrollViewListener {

    void onScrollChanged(UnaiMasaBlogScrollView scrollView, int x, int y, int oldx, int oldy);

}

package com.unaimasa.unaimasablog.entity.rest;

import java.util.ArrayList;

/**
 * Created by unai.masa on 19/05/2016.
 */
public class BlogmediaPublishedCollection extends ArrayList<Blogmedia> {

    private ArrayList<Blogmedia> items = this;

    public ArrayList<Blogmedia> getItems() {
        return items;
    }

    public void setItems(ArrayList<Blogmedia> result) {
        if (result != null) {
            this.clear();
            this.addAll(result);
        }
    }

    public BlogmediaPublishedCollection() {
    }
}

package com.unaimasa.unaimasablog.entity;

/**
 * Created by unai.masa on 06/06/2016.
 */
public class Project {

    private String mId;

    private String mTitle;

    private String mImage_url;

    private String mLink;

    private ProjectPlatform mProjectPlatform;

    public Project(String title, String image_url, String link, ProjectPlatform projectPlatform){
        this.mTitle = title;
        this.mImage_url = image_url;
        this.mLink = link;
        this.mProjectPlatform = projectPlatform;
    }

    public String getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getImageUrl() {
        return mImage_url;
    }

    public String getLink() { return mLink; }

    public ProjectPlatform getProjectPlatform() { return mProjectPlatform; }

    public enum ProjectPlatform {
        ANDROID, APPLE, BOTH, MULTIPLATFORM
    }
}

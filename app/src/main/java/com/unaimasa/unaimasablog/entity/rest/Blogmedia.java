package com.unaimasa.unaimasablog.entity.rest;

/**
 * Created by unai.masa on 19/05/2016.
 */
public class Blogmedia {

    private String id;

    private String title;

    private String date;

    private String video_url;

    private String first_paragraph;

    private String subtitle;

    private String instagram_image_url;

    private String image_url;

    private String second_paragraph;

    private String thanks_paragraph;

    private String published;

    public Blogmedia() {
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public String getVideoUrl() {
        return video_url;
    }

    public String getFirstParagraph() {
        return first_paragraph;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getInstagramImageUrl() {
        return instagram_image_url;
    }

    public String getImageUrl() {
        return image_url;
    }

    public String getSecondParagraph() {
        return second_paragraph;
    }

    public String getThanksParagraph() {
        return thanks_paragraph;
    }

    public String getPublished() {
        return published;
    }

    @Override
    public String toString() {
        return "Blogmedia{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", date='" + date + '\'' +
                ", video_url='" + video_url + '\'' +
                ", first_paragraph='" + first_paragraph + '\'' +
                ", subtitle='" + subtitle + '\'' +
                ", instagram_image_url='" + instagram_image_url + '\'' +
                ", image_url='" + image_url + '\'' +
                ", second_paragraph='" + second_paragraph + '\'' +
                ", thanks_paragraph='" + thanks_paragraph + '\'' +
                ", published='" + published + '\'' +
                '}';
    }
}

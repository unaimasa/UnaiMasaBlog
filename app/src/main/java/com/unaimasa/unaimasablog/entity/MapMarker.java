package com.unaimasa.unaimasablog.entity;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by unai.masa on 07/06/2016.
 */
public class MapMarker {

    private String mId;

    private String mLocation;

    private double mLatitude;

    private double mLongitude;

    private MarkerType mMarkerType;

    public MapMarker(String location, double latitude, double longitude, MarkerType markerType){
        this.mLocation = location;
        this.mLatitude = latitude;
        this.mLongitude = longitude;
        this.mMarkerType = markerType;
    }

    public String getId() {
        return mId;
    }

    public String getLocation() {
        return mLocation;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public double getLongitude() { return mLongitude; }

    public MarkerType getMarkerType() { return mMarkerType; }

    public LatLng getPosition() {
        return new LatLng(getLatitude(), getLongitude());
    }

    public enum MarkerType {
        CURRENT, VISITED
    }

}

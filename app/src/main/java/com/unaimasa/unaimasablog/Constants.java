package com.unaimasa.unaimasablog;

import android.content.ContentResolver;
import android.net.Uri;


/**
 * Created by unai.masa on 05/04/2016.
 */
public class Constants {
    public static final String EMPTY_STRING = "";
    public static final String HTTP_PATH_SEPARATOR = "/";
    public static final String ENDPOINT_PARAM_TEMPLATE = "(.*)";
    public static final String TOKEN_UNAI_MASA_BLOG = "unai_masa_blog_token";


    public interface Header {
        String ETAG = "ETag";
        String TOKEN = "token";
        String IF_NON_MATCH = "If-None-Match";
        String EXPIRED_TIME = "Cache-Control";
    }

    public interface API_Return_Format {
        String JSON = "json";
        String XML = "xml";
        String HTML = "html";
    }

    public interface SharedKeys {
        // user
        String USER_NOTIFICATION = "user_notification";

        // blog
        String BLOG_POSITION = "blog_position";

        // location
        String MAP_TYPE = "map_type";

        // etag
        String ETAG_VALUE = "etag_value";

        // Language Selector
        String LANGUAGE_SELECTED_CODE = "language_selected_code";
    }

    public interface Errors {
        int SPICE_UNKNOWN_EXCEPTION = -1;
        int NETWORK_PROBLEM_EXCEPTION = -2;
        int SERVER_304_EXCEPTION = 304;
        int SERVER_400_EXCEPTION = 400;
        int SERVER_401_EXCEPTION = 401;
        int SERVER_402_EXCEPTION = 402;
        int SERVER_403_EXCEPTION = 403;
        int SERVER_404_EXCEPTION = 404;
        int SERVER_405_EXCEPTION = 405;
        int SERVER_409_EXCEPTION = 409;
        int SERVER_500_EXCEPTION = 500;

        String GENERIC_ERROR = "generic_error";
        String NETWORK_PROBLEM_ERROR = "network_problem_error";
    }

    public interface LoadersIds {
        int BLOGMEDIA_PUBLISHED_LOADER_ID = 0x0001;
    }

    public interface Config {
        boolean DEVELOPER_MODE = true;
    }

    public interface SocialNetworksPackages {
        String YOUTUBE_PACKAGE = "com.google.android.youtube";
        String INSTAGRAM_PACKAGE = "com.instagram.android";
        String TWITTER_PACKAGE = "com.twitter.android";
        String LINKEDIN_PACKAGE = "com.linkedin.android";
    }

    public interface SocialNetworks {
        String YOUTUBE_CHANNEL = "https://www.youtube.com/channel/UC6-7TD4njA4i0exfCcxx6Cg";
        String INSTAGRAM_CHANNEL = "http://www.instagram.com/megapixelboy/";
        String TWITTER_CHANNEL = "https://twitter.com/MegaPixelBoy";
        String GITHUB_CHANNEL = "https://gitlab.com/u/unaimasa";
        String LINKEDIN_CHANNEL = "https://www.linkedin.com/in/unai-masa-sanchez-2b194996";
    }

    public interface AppPhotos {
        // Portfolio
        String PORTFOLIO_PHOTO_PROJECT_CHICKEN_RAIN = "http://unaimasa.com/pix/projects/project_chicken_rain.png";
        String PORTFOLIO_PHOTO_PROJECT_SAVE_A_NOTE = "http://unaimasa.com/pix/projects/project_san.png";
        String PORTFOLIO_PHOTO_PROJECT_PIXEL_UDP_CHAT = "http://unaimasa.com/pix/projects/project_pixel_udp_chat.png";
        String PORTFOLIO_PHOTO_PROJECT_BLE = "http://unaimasa.com/pix/projects/project_ble.png";
        String PORTFOLIO_PHOTO_PROJECT_LIGHTS_MANAGEMENT = "http://unaimasa.com/pix/projects/project_lights_management.png";
        String PORTFOLIO_PHOTO_PROJECT_SHOPS_MANAGEMENT = "http://unaimasa.com/pix/projects/project_shops_management.png";
        String PORTFOLIO_PHOTO_PROJECT_VEHICLES_MANAGEMENT = "http://unaimasa.com/pix/projects/project_vehicles_management.png";

        String PORTFOLIO_PHOTO_1 = "http://unaimasa.com/pix/unai/portfolio/big_ben_portfolio_photo.jpg";
        String PORTFOLIO_PHOTO_2 = "http://unaimasa.com/pix/unai/portfolio/london_bridge_portfolio_photo.jpg";
        String PORTFOLIO_PHOTO_3 = "http://unaimasa.com/pix/unai/portfolio/saint_paul_cathedral_portfolio_photo.jpg";

        // CV
        String CV_SKILL_ANDROID = "http://unaimasa.com/pix/icons/color/24px/Android.png";
        String CV_SKILL_HTML5 = "http://unaimasa.com/pix/icons/color/24px/Html5.png";
        String CV_SKILL_JAVASCRIPT = "http://unaimasa.com/pix/icons/color/24px/Javascript.png";
        String CV_SKILL_PHONEGAP = "http://unaimasa.com/pix/icons/color/24px/Phonegap.png";
        String CV_SKILL_JQUERY = "http://unaimasa.com/pix/icons/color/24px/Jquery.png";
        String CV_SKILL_PHP = "http://unaimasa.com/pix/icons/color/24px/Php.png";
        String CV_SKILL_MYSQL = "http://unaimasa.com/pix/icons/color/24px/Mysql.png";
        String CV_SKILL_SCRUM = "http://unaimasa.com/pix/icons/color/24px/Scrum.png";
        String CV_SKILL_POMODORO = "http://unaimasa.com/pix/icons/color/24px/Pomodoro.png";
        String CV_SKILL_IOS = "http://unaimasa.com/pix/icons/color/24px/Apple_1.png";
        String CV_SKILL_PYTHON = "http://unaimasa.com/pix/icons/color/24px/Python.png";
        String CV_SKILL_CPLUSPLUS = "http://unaimasa.com/pix/icons/color/24px/Cplusplus.png";

        String CV_EXPERIENCE_PICSOLVE = "http://unaimasa.com/pix/icons/company/picsolve_logo.jpeg";
        String CV_EXPERIENCE_TST = "http://unaimasa.com/pix/icons/company/tst_logo.jpg";
        String CV_EXPERIENCE_BYV = "http://unaimasa.com/pix/icons/company/byv_logo.png";
        String CV_EXPERIENCE_BATURA = "http://unaimasa.com/pix/icons/company/batura_logo.png";

        String CV_STUDY_EHU = "http://unaimasa.com/pix/icons/education/upv_logo.jpg";
        String CV_STUDY_USM = "http://unaimasa.com/pix/icons/education/usm_logo.jpg";
        String CV_STUDY_HEOI = "http://unaimasa.com/pix/icons/education/heoi_logo.jpg";

        // About
        String ABOUT_PHOTO = "http://unaimasa.com/pix/unai/about/unai_masa_about_photo.jpg";
    }

    public interface LanguageCodes {
            String SPANISH_LANGUAGE_CODE = "es";
            String ENGLISH_LANGUAGE_CODE = "en";
    }

    public interface AdMob_Configuration {
        String APP_ID = "ca-app-pub-3507235862661537~3191609606";
    }

}

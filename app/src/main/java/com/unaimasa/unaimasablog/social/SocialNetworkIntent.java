package com.unaimasa.unaimasablog.social;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import com.unaimasa.unaimasablog.Constants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * Created by unai.masa on 18/04/2016.
 */
public class SocialNetworkIntent {

    private static final Logger sLogger = LoggerFactory.getLogger(SocialNetworkIntent.class);

    // ===== Youtube Intent ===== //
    public static Intent newYoutubeProfileIntent(PackageManager pm, String url) {
        sLogger.info("Social Network Intent Called - {}", url);
        final Intent intentYoutube = new Intent(Intent.ACTION_VIEW);
        try {
            intentYoutube.setPackage(Constants.SocialNetworksPackages.YOUTUBE_PACKAGE);
            intentYoutube.setData(Uri.parse(Constants.SocialNetworks.YOUTUBE_CHANNEL));
            return intentYoutube;
        } catch (ActivityNotFoundException e) {
            intentYoutube.setData(Uri.parse(Constants.SocialNetworks.YOUTUBE_CHANNEL));
        }
        return intentYoutube;
    }

    // ===== Instagram Intent ===== //
    // http://stackoverflow.com/questions/21505941/intent-to-open-instagram-user-profile-on-android
    public static Intent newInstagramProfileIntent(PackageManager pm, String url) {
        sLogger.info("Social Network Intent Called - {}", url);
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        try {
            if (pm.getPackageInfo(Constants.SocialNetworksPackages.INSTAGRAM_PACKAGE, 0) != null) {
                if (url.endsWith("/")) {
                    url = url.substring(0, url.length() - 1);
                }
                final String username = url.substring(url.lastIndexOf("/") + 1);
                intent.setData(Uri.parse("http://instagram.com/_u/" + username));
                intent.setPackage(Constants.SocialNetworksPackages.INSTAGRAM_PACKAGE);
                return intent;
            }
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        intent.setData(Uri.parse(url));
        return intent;
    }

    // ===== Twitter Intent ===== //
    public static Intent newTwitterProfileIntent(PackageManager pm, String url) {
        sLogger.info("Social Network Intent Called - {}", url);
        final Intent intentTwitter = new Intent(Intent.ACTION_VIEW);
        try {
            intentTwitter.setPackage(Constants.SocialNetworksPackages.TWITTER_PACKAGE);
            intentTwitter.setData(Uri.parse(Constants.SocialNetworks.TWITTER_CHANNEL));
            return intentTwitter;
        } catch (ActivityNotFoundException e) {
            intentTwitter.setData(Uri.parse(Constants.SocialNetworks.TWITTER_CHANNEL));
        }
        return intentTwitter;
    }

    // ===== Twitter Intent ===== //
    public static Intent newGithubProfileIntent(PackageManager pm, String url) {
        sLogger.info("Social Network Intent Called - {}", url);
        Intent intentGithub = new Intent(Intent.ACTION_VIEW);
        intentGithub.setData(Uri.parse(Constants.SocialNetworks.GITHUB_CHANNEL));
        return intentGithub;
    }

    // ===== Linkedin Intent ===== //
    public static Intent newLinkedinProfileIntent(PackageManager pm, String url) {
        sLogger.info("Social Network Intent Called - {}", url);
        final Intent intentLinkedin = new Intent(Intent.ACTION_VIEW);
        try {
            intentLinkedin.setPackage(Constants.SocialNetworksPackages.LINKEDIN_PACKAGE);
            intentLinkedin.setData(Uri.parse(Constants.SocialNetworks.LINKEDIN_CHANNEL));
            return intentLinkedin;
        } catch (ActivityNotFoundException e) {
            intentLinkedin.setData(Uri.parse(Constants.SocialNetworks.LINKEDIN_CHANNEL));
        }
        return intentLinkedin;
    }

}

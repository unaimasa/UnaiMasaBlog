package com.unaimasa.unaimasablog.social;

import android.content.Intent;

import com.unaimasa.unaimasablog.BuildConfig;
import com.unaimasa.unaimasablog.Constants;
import com.unaimasa.unaimasablog.UnaiMasaBlogTestRunner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;

/**
 * Created by unai.masa on 13/06/2016.
 */
@RunWith(UnaiMasaBlogTestRunner.class)
@Config(constants = BuildConfig.class)
public class TestSocialNetworkIntent {

    @Before
    public void setUpSocialNetworkIntent() {
    }

    @Test
    public void test_Intent_Should_Lunch_NewYoutubeProfileIntent(){

    }

}

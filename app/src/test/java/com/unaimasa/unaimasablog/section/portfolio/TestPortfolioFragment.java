package com.unaimasa.unaimasablog.section.portfolio;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.unaimasa.unaimasablog.BuildConfig;
import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.UnaiMasaBlogTestRunner;
import com.unaimasa.unaimasablog.entity.Project;
import com.unaimasa.unaimasablog.section.core.activity.MainActivityTestImpl;

import junit.framework.TestCase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import java.util.ArrayList;

/**
 * Created by unai.masa on 21/06/2016.
 */
@RunWith(UnaiMasaBlogTestRunner.class)
@Config(constants = BuildConfig.class)
public class TestPortfolioFragment {

    private MainActivityTestImpl mMainActivity;
    private PortfolioFragment mPortfolioFragment;
    private ArrayList<Project> mProjects;

    @Before
    public void setup() {
        mMainActivity = Robolectric.buildActivity(MainActivityTestImpl.class)
                .create(new Bundle())
                .start()
                .resume()
                .visible()
                .get();
        mPortfolioFragment = new PortfolioFragment();
        SupportFragmentTestUtil.startFragment(mPortfolioFragment, MainActivityTestImpl.class);
    }

    @Test
    public void testCreateAboutFragment(){
        TestCase.assertEquals(mPortfolioFragment.getClass().getSimpleName(), PortfolioFragment.class.getSimpleName());
    }

    @Test
    public void testGetCustomLayoutId() {
        TestCase.assertEquals(mPortfolioFragment.getCustomLayoutId(), R.layout.fragment_portfolio);
    }

    @Test
    public void testGetPortfolioElementsNotNull() {
        Toolbar mToolbar = (Toolbar) mMainActivity.findViewById(R.id.toolbar);
        TextView mTVPortfolioIntroduction = (TextView) mPortfolioFragment.getView().findViewById(R.id.tv_portfolio_introduction);

        ImageView mIVPortfolio1 = (ImageView) mPortfolioFragment.getView().findViewById(R.id.iv_portfolio_image_1);
        TextView mTVPortfolioTitle1 = (TextView) mPortfolioFragment.getView().findViewById(R.id.tv_portfolio_title_1);
        TextView mTVPortfolioText1 = (TextView) mPortfolioFragment.getView().findViewById(R.id.tv_portfolio_text_1);

        ImageView mIVPortfolio2 = (ImageView) mPortfolioFragment.getView().findViewById(R.id.iv_portfolio_image_2);
        TextView mTVPortfolioTitle2 = (TextView) mPortfolioFragment.getView().findViewById(R.id.tv_portfolio_title_2);
        TextView mTVPortfolioText2 = (TextView) mPortfolioFragment.getView().findViewById(R.id.tv_portfolio_text_2);

        ImageView mIVPortfolio3 = (ImageView) mPortfolioFragment.getView().findViewById(R.id.iv_portfolio_image_3);
        TextView mTVPortfolioTitle3 = (TextView) mPortfolioFragment.getView().findViewById(R.id.tv_portfolio_title_3);
        TextView mTVPortfolioText3 = (TextView) mPortfolioFragment.getView().findViewById(R.id.tv_portfolio_text_3);


        Assert.assertNotNull(mToolbar);
        Assert.assertNotNull(mTVPortfolioIntroduction);

        Assert.assertNotNull(mIVPortfolio1);
        Assert.assertNotNull(mTVPortfolioTitle1);
        Assert.assertNotNull(mTVPortfolioText1);

        Assert.assertNotNull(mIVPortfolio2);
        Assert.assertNotNull(mTVPortfolioTitle2);
        Assert.assertNotNull(mTVPortfolioText2);

        Assert.assertNotNull(mIVPortfolio3);
        Assert.assertNotNull(mTVPortfolioTitle3);
        Assert.assertNotNull(mTVPortfolioText3);
    }

    @Test
    public void testGetProjects() {
        mProjects = mPortfolioFragment.getProjects();
        TestCase.assertNotNull(mProjects);
    }

}

package com.unaimasa.unaimasablog.section.cv;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.unaimasa.unaimasablog.BuildConfig;
import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.UnaiMasaBlogTestRunner;
import com.unaimasa.unaimasablog.section.core.activity.MainActivityTestImpl;

import junit.framework.TestCase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

/**
 * Created by unai.masa on 21/06/2016.
 */
@RunWith(UnaiMasaBlogTestRunner.class)
@Config(constants = BuildConfig.class)
public class TestCVFragment {

    private MainActivityTestImpl mMainActivity;
    private CVFragment mCVFragment;

    @Before
    public void setup() {
        mMainActivity = Robolectric.buildActivity(MainActivityTestImpl.class)
                .create(new Bundle())
                .start()
                .resume()
                .visible()
                .get();
        mCVFragment = new CVFragment();
        SupportFragmentTestUtil.startFragment(mCVFragment, MainActivityTestImpl.class);
    }

    @Test
    public void testCreateAboutFragment(){
        TestCase.assertEquals(mCVFragment.getClass().getSimpleName(), CVFragment.class.getSimpleName());
    }

    @Test
    public void testGetCustomLayoutId() {
        TestCase.assertEquals(mCVFragment.getCustomLayoutId(), R.layout.fragment_cv);
    }

    @Test
    public void testGetAboutElementsNotNull() {
        Toolbar mToolbar = (Toolbar) mMainActivity.findViewById(R.id.toolbar);
        ImageView mIVSkillAndroid = (ImageView) mCVFragment.getView().findViewById(R.id.iv_skill_android);
        ImageView mIVSkillHtml5 = (ImageView) mCVFragment.getView().findViewById(R.id.iv_skill_html5);
        ImageView mIVSkillJavascript = (ImageView) mCVFragment.getView().findViewById(R.id.iv_skill_javascript);
        ImageView mIVSkillPhonegap = (ImageView) mCVFragment.getView().findViewById(R.id.iv_skill_phonegap);
        ImageView mIVSkillJquery = (ImageView) mCVFragment.getView().findViewById(R.id.iv_skill_jquery);
        ImageView mIVSkillPhp = (ImageView) mCVFragment.getView().findViewById(R.id.iv_skill_php);
        ImageView mIVSkillMysql = (ImageView) mCVFragment.getView().findViewById(R.id.iv_skill_mysql);
        ImageView mIVSkillScrum = (ImageView) mCVFragment.getView().findViewById(R.id.iv_skill_scrum);
        ImageView mIVSkillPomodoro = (ImageView) mCVFragment.getView().findViewById(R.id.iv_skill_pomodoro);
        ImageView mIVSkillIos = (ImageView) mCVFragment.getView().findViewById(R.id.iv_skill_ios);
        ImageView mIVSkillPython = (ImageView) mCVFragment.getView().findViewById(R.id.iv_skill_python);
        ImageView mIVSkillCPlusPlus = (ImageView) mCVFragment.getView().findViewById(R.id.iv_skill_cplusplus);
        ImageView mIVExperiencePicsolve = (ImageView) mCVFragment.getView().findViewById(R.id.iv_experience_picsolve);
        ImageView mIVExperienceTst = (ImageView) mCVFragment.getView().findViewById(R.id.iv_experience_tst);
        ImageView mIVExperienceByv = (ImageView) mCVFragment.getView().findViewById(R.id.iv_experience_byv);
        ImageView mIVExperienceBatura = (ImageView) mCVFragment.getView().findViewById(R.id.iv_experience_batura);
        ImageView mIVStudyDegree1 = (ImageView) mCVFragment.getView().findViewById(R.id.iv_study_degree_1);
        ImageView mIVStudyDegree2 = (ImageView) mCVFragment.getView().findViewById(R.id.iv_study_degree_2);
        ImageView mIVStudyMaster1 = (ImageView) mCVFragment.getView().findViewById(R.id.iv_study_master_1);

        Assert.assertNotNull(mToolbar);
        Assert.assertNotNull(mIVSkillAndroid);
        Assert.assertNotNull(mIVSkillHtml5);
        Assert.assertNotNull(mIVSkillJavascript);
        Assert.assertNotNull(mIVSkillPhonegap);
        Assert.assertNotNull(mIVSkillJquery);
        Assert.assertNotNull(mIVSkillPhp);
        Assert.assertNotNull(mIVSkillMysql);
        Assert.assertNotNull(mIVSkillScrum);
        Assert.assertNotNull(mIVSkillPomodoro);
        Assert.assertNotNull(mIVSkillIos);
        Assert.assertNotNull(mIVSkillPython);
        Assert.assertNotNull(mIVSkillCPlusPlus);
        Assert.assertNotNull(mIVExperiencePicsolve);
        Assert.assertNotNull(mIVExperienceTst);
        Assert.assertNotNull(mIVExperienceByv);
        Assert.assertNotNull(mIVExperienceBatura);
        Assert.assertNotNull(mIVStudyDegree1);
        Assert.assertNotNull(mIVStudyDegree2);
        Assert.assertNotNull(mIVStudyMaster1);
    }

}

package com.unaimasa.unaimasablog.section.map;

import android.os.Bundle;

import com.unaimasa.unaimasablog.BuildConfig;
import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.UnaiMasaBlogTestRunner;
import com.unaimasa.unaimasablog.section.core.activity.MainActivityTestImpl;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

/**
 * Created by unai.masa on 22/06/2016.
 */
@RunWith(UnaiMasaBlogTestRunner.class)
@Config(constants = BuildConfig.class)
public class TestLocationFragment {

    private MainActivityTestImpl mMainActivity;
    private LocationFragment mLocationFragment;

    @Before
    public void setup() {
        mMainActivity = Robolectric.buildActivity(MainActivityTestImpl.class)
                .create(new Bundle())
                .start()
                .resume()
                .visible()
                .get();
        mLocationFragment = new LocationFragment();
        SupportFragmentTestUtil.startFragment(mLocationFragment, MainActivityTestImpl.class);
    }


    @Test
    public void testCreateAboutFragment(){
        TestCase.assertEquals(mLocationFragment.getClass().getSimpleName(), LocationFragment.class.getSimpleName());
    }

    @Test
    public void testGetCustomLayoutId() {
        TestCase.assertEquals(mLocationFragment.getCustomLayoutId(), R.layout.fragment_map);
    }


}

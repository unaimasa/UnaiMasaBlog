package com.unaimasa.unaimasablog.section.core.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;

import com.unaimasa.unaimasablog.BuildConfig;
import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.UnaiMasaBlogTestRunner;
import com.unaimasa.unaimasablog.section.blog.fragment.BlogFragment;
import com.unaimasa.unaimasablog.section.core.activity.MainActivity;
import com.unaimasa.unaimasablog.section.core.activity.MainActivityTestImpl;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.annotation.Config;

/**
 * Created by unai.masa on 21/06/2016.
 */
@RunWith(UnaiMasaBlogTestRunner.class)
@Config(constants = BuildConfig.class)
public class TestMainActivity {

    private MainActivity mMainActivity;

    @Before
    public void setup() {
        mMainActivity = Robolectric.buildActivity(MainActivityTestImpl.class)
                .create(new Bundle())
                .start()
                .resume()
                .visible()
                .get();
    }

    @Test
    public void testCreateMainActivity() {
        Fragment fragment = mMainActivity.getSupportFragmentManager().findFragmentById(R.id.container_fragments);
        TestCase.assertNotNull(fragment);
        TestCase.assertEquals(fragment.getClass().getSimpleName(), BlogFragment.class.getSimpleName());
    }
}

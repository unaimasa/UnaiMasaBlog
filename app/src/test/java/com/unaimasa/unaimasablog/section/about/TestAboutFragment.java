package com.unaimasa.unaimasablog.section.about;


import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.unaimasa.unaimasablog.BuildConfig;
import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.UnaiMasaBlogTestRunner;
import com.unaimasa.unaimasablog.section.core.activity.MainActivityTestImpl;

import junit.framework.TestCase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

/**
 * Created by unai.masa on 21/06/2016.
 */
@RunWith(UnaiMasaBlogTestRunner.class)
@Config(constants = BuildConfig.class)
public class TestAboutFragment {

    private MainActivityTestImpl mMainActivity;
    private AboutFragment mAboutFragment;

    @Before
    public void setup() {
        mMainActivity = Robolectric.buildActivity(MainActivityTestImpl.class)
                .create(new Bundle())
                .start()
                .resume()
                .visible()
                .get();
        mAboutFragment = new AboutFragment();
        SupportFragmentTestUtil.startFragment(mAboutFragment, MainActivityTestImpl.class);
    }

    @Test
    public void testCreateAboutFragment(){
        TestCase.assertEquals(mAboutFragment.getClass().getSimpleName(), AboutFragment.class.getSimpleName());
    }

    @Test
    public void testGetCustomLayoutId() {
        TestCase.assertEquals(mAboutFragment.getCustomLayoutId(), R.layout.fragment_about);
    }

    @Test
    public void testGetAboutElementsNotNull() {
        Toolbar mToolbar = (Toolbar) mMainActivity.findViewById(R.id.toolbar);
        ImageView mIVAbout1 = (ImageView) mAboutFragment.getView().findViewById(R.id.iv_about_image_1);
        TextView mTVAboutText1 = (TextView) mAboutFragment.getView().findViewById(R.id.tv_about_text_1);

        Assert.assertNotNull(mToolbar);
        Assert.assertNotNull(mIVAbout1);
        Assert.assertNotNull(mTVAboutText1);
    }
}

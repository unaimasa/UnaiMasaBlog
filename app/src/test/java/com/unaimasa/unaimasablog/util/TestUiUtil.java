package com.unaimasa.unaimasablog.util;

import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;

import com.unaimasa.unaimasablog.BuildConfig;
import com.unaimasa.unaimasablog.UnaiMasaBlogTestRunner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;

/**
 * Created by unai.masa on 07/12/2015.
 */
@RunWith(UnaiMasaBlogTestRunner.class)
@Config(constants = BuildConfig.class)
public class TestUiUtil {

    private AppCompatActivity activity;

    /*
    @Test
    public void testShouldHideSoftKeyboard(){

    }

    @Test
    public void testShouldShowSoftKeyboard(){

    }
    */

    @Test
    public void testShouldGetDeviceWithInDp() {
        float width = UiUtil.getDeviceWidthInDp();
        assertThat(width, not(nullValue()));
        assertThat(width, is(480f));
    }

    @Test
    public void testShouldGetDeviceHeightInDp() {
        float height = UiUtil.getDeviceHeightInDp();
        assertThat(height, not(nullValue()));
        assertThat(height, is(800f));
    }

    @Test
    public void testShouldGetDeviceWithInPx() {
        float width = UiUtil.getDeviceWidthInPx();
        assertThat(width, not(nullValue()));
        assertThat(width, is(480f));
    }

    @Test
    public void testShouldGetDeviceHeightInPx() {
        float height = UiUtil.getDeviceHeightInPx();
        assertThat(height, not(nullValue()));
        assertThat(height, is(800f));
    }

    @Test
    public void testShouldGetDisplayMetrix(){
        DisplayMetrics metrix = UiUtil.getDisplayMetrics();
        assertThat(metrix, not(nullValue()));
    }

    @Test
    public void testShouldGetDisplayDensity(){
        float density = UiUtil.getDisplayDensity();
        assertThat(density, not(nullValue()));
        assertThat(density, is(1f));
    }

    @Test
    public void testShouldConvertDpToPx() {
        float dp = 100f;
        int px = UiUtil.convertDpToPx(dp);
        assertThat(px, is(100));
    }

}

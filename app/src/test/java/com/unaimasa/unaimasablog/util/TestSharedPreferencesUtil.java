package com.unaimasa.unaimasablog.util;

import com.unaimasa.unaimasablog.BuildConfig;
import com.unaimasa.unaimasablog.Constants;
import com.unaimasa.unaimasablog.UnaiMasaBlogTestRunner;

import org.hamcrest.core.IsEqual;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;

/**
 * Created by unai.masa on 19/01/2016.
 */
@RunWith(UnaiMasaBlogTestRunner.class)
@Config(constants = BuildConfig.class)
public class TestSharedPreferencesUtil {

    public static final String TEST_KEY = "TEST_KEY";

    @Before
    public void setUp() {
        SharedPreferencesUtil.getInstance().clearAll();
    }

    @Test
    public void testShouldGetInstance(){
        SharedPreferencesUtil prefs = SharedPreferencesUtil.getInstance();
        assertThat(prefs, not(nullValue()));
    }

    @Test
    public void testShouldPutString(){
        SharedPreferencesUtil prefs = SharedPreferencesUtil.getInstance();
        String value = "value";
        prefs.putString(TEST_KEY, value);
        String valueFromPrefs = prefs.getString(TEST_KEY);
        assertThat(value, equalTo(valueFromPrefs));
    }

    @Test
    public void testShouldGetString(){
        SharedPreferencesUtil prefs = SharedPreferencesUtil.getInstance();
        prefs.clearValue(TEST_KEY);
        String valueFromPrefs = prefs.getString(TEST_KEY, "");
        assertThat(valueFromPrefs, is(Constants.EMPTY_STRING));
    }

    @Test
    public void testShouldPutInt(){
        SharedPreferencesUtil prefs = SharedPreferencesUtil.getInstance();
        int value = 1234567890;
        prefs.putInt(TEST_KEY, value);
        int valueFromPrefs = prefs.getInt(TEST_KEY);
        assertThat(value, equalTo(valueFromPrefs));
    }

    @Test
    public void testShouldGetInt(){
        SharedPreferencesUtil prefs = SharedPreferencesUtil.getInstance();
        prefs.clearValue(TEST_KEY);
        int valueFromPrefs = prefs.getInt(TEST_KEY, -1);
        assertThat(valueFromPrefs, is(-1));
    }

    @Test
    public void testShouldPutFloat(){
        SharedPreferencesUtil prefs = SharedPreferencesUtil.getInstance();
        float value = 1234567890f;
        prefs.putFloat(TEST_KEY, value);
        float valueFromPrefs = prefs.getFloat(TEST_KEY, -1f);
        assertThat(value, equalTo(valueFromPrefs));
    }

    @Test
    public void testShouldGetFloat(){
        SharedPreferencesUtil prefs = SharedPreferencesUtil.getInstance();
        prefs.clearValue(TEST_KEY);
        float valueFromPrefs = prefs.getFloat(TEST_KEY, -1f);
        assertThat(valueFromPrefs, is(-1f));
    }

    @Test
    public void testShouldPutBoolean(){
        SharedPreferencesUtil prefs = SharedPreferencesUtil.getInstance();
        boolean value = true;
        prefs.putBool(TEST_KEY, value);
        boolean valueFromPrefs = prefs.getBoolean(TEST_KEY);
        assertThat(value, equalTo(valueFromPrefs));
    }

    @Test
    public void testShouldGetBoolean(){
        SharedPreferencesUtil prefs = SharedPreferencesUtil.getInstance();
        prefs.clearValue(TEST_KEY);
        boolean valueFromPrefs = prefs.getBoolean(TEST_KEY, true);
        assertThat(valueFromPrefs, is(true));
    }

    @Test
    public void testShouldPutStringSet(){
        SharedPreferencesUtil prefs = SharedPreferencesUtil.getInstance();
        Set<String> value = new HashSet<>();
        value.add("value1");
        value.add("value2");
        prefs.putStringSet(TEST_KEY, value);
        Set<String> valuefromPrefs = prefs.getStringSet(TEST_KEY);
        assertThat(valuefromPrefs, notNullValue());
        assertThat(value, IsEqual.equalTo(valuefromPrefs));
        assertThat(value.size(), IsEqual.equalTo(valuefromPrefs.size()));
    }

    @After
    public void tearDown() { }
}

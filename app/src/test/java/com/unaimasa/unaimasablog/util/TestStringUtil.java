package com.unaimasa.unaimasablog.util;

import com.unaimasa.unaimasablog.BuildConfig;
import com.unaimasa.unaimasablog.UnaiMasaBlogTestRunner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Created by unai.masa on 13/06/2016.
 */
@RunWith(UnaiMasaBlogTestRunner.class)
@Config(constants = BuildConfig.class)
public class TestStringUtil {

    @Test
    public void testFormatNoElement(){
        String pathTemplate = "pi/blogmedia/published";
        String[] params = new String[]{};
        String result = "pi/blogmedia/published";
        assertThat(result, is(StringUtil.format(pathTemplate, params)));
    }

    @Test
    public void testFormatElement(){
        String pathTemplate = "api/blogmedia/(.*)";
        String[] params = new String[]{"1"};
        String result = "api/blogmedia/1";
        assertThat(result, is(StringUtil.format(pathTemplate, params)));
    }

}

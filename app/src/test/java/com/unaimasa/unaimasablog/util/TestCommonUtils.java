package com.unaimasa.unaimasablog.util;


import com.unaimasa.unaimasablog.BuildConfig;
import com.unaimasa.unaimasablog.R;
import com.unaimasa.unaimasablog.UnaiMasaBlogApp;
import com.unaimasa.unaimasablog.UnaiMasaBlogTestRunner;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowToast;

/**
 * Created by unai.masa on 07/12/2015.
 */
@RunWith(UnaiMasaBlogTestRunner.class)
@Config(constants = BuildConfig.class)
public class TestCommonUtils extends CommonUtils {

    public static final String TEST_STRING = "test string";

    @Test
    public void testShouldShowToast(){
        CommonUtils.showToast(TEST_STRING);
        Assert.assertEquals("test string", ShadowToast.getTextOfLatestToast());
    }

    @Test
    public void testShouldShowToastFromString(){
        String expected = UnaiMasaBlogApp.getInstance().getString(R.string.app_name);
        CommonUtils.showToast(R.string.app_name);
        Assert.assertEquals(expected, ShadowToast.getTextOfLatestToast());
    }

}

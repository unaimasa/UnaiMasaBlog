package com.unaimasa.unaimasablog.util.media;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;
import com.unaimasa.unaimasablog.BuildConfig;
import com.unaimasa.unaimasablog.UnaiMasaBlogApp;
import com.unaimasa.unaimasablog.UnaiMasaBlogTestRunner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.httpclient.FakeHttp;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.CountDownLatch;

import okio.Buffer;
import okio.BufferedSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;

/**
 * Created by unai.masa on 13/06/2016.
 */
@RunWith(UnaiMasaBlogTestRunner.class)
@Config(constants = BuildConfig.class)
public class TestUIL {

    @Test
    public void testUIL() throws Exception {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(UnaiMasaBlogApp.getInstance())
                //.memoryCacheExtraOptions(480, 800) // default = device screen dimensions
                //.diskCacheExtraOptions(480, 800, null)
                .threadPoolSize(3) // default
                .threadPriority(Thread.NORM_PRIORITY) // default
                .tasksProcessingOrder(QueueProcessingType.LIFO) // default
                .denyCacheImageMultipleSizesInMemory()
                .memoryCacheSize(2 * 1024 * 1024)
                .writeDebugLogs()
                .build();
        ImageLoader.getInstance().destroy();
        ImageLoader.getInstance().init(config);
        FakeHttp.addPendingHttpResponse(404, "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq");
        final String url = "http://common.com/test_URL_FOR_IMAGE.png";
        final String expected = "http://common.com/test_URL_FOR_IMAGE_100x100.png";
        final CountDownLatch latch = new CountDownLatch(1);
        ImageView view = new ImageView(UnaiMasaBlogApp.getInstance());
        view.setScaleType(ImageView.ScaleType.CENTER);
        UIL.bind(url, view)
                .setAlpha(50f)
                .setHeightDp(50)
                .setWidthDp(50)
                .setHeightPx(100)
                .setWidthPx(100)
                .setShouldResize(true)
                .setLoadingListener(new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        assertThat(expected, is(imageUri));
                        latch.countDown();
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        assertThat(expected, is(imageUri));
                        assertThat(failReason, notNullValue());
                        latch.countDown();
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        latch.countDown();
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        latch.countDown();
                    }
                })
                .load();
        latch.await();
    }

    @Test
    public void testConfig() {
        ImageLoaderConfigurator.setup(UnaiMasaBlogApp.getInstance());
        assertThat(ImageLoader.getInstance().isInited(), is(true));
    }

    @Test
    public void testDefOptions() {
        DisplayImageOptions.Builder builder = ImageLoaderConfigurator.getDefaultOptionsBuilder();
        assertThat(builder, notNullValue());
        assertThat(builder.build().isCacheInMemory(), is(true));
        assertThat(builder.build().isCacheOnDisk(), is(true));
    }


    @Test
    public void testStream() throws Exception {
        OkHttpClient client = getClient();
        InputStream stream = new OkHttpImageDownloader(UnaiMasaBlogApp.getInstance(), client).getStreamFromNetwork("http://common.com/test", null);
        assertThat(stream, notNullValue());
        assertThat(stream.available(), not(0));
        assertThat(stream.read(), not(0));
    }

    private OkHttpClient getClient() {
        final InputStream stream = new ByteArrayInputStream("response string".getBytes());
        OkHttpClient client = new OkHttpClient();
        client.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                return new Response.Builder()
                        .protocol(Protocol.HTTP_2)
                        // This is essential as it makes response.isSuccessful() returning true.
                        .code(200)
                        .request(chain.request())
                        .body(new ResponseBody() {
                            @Override
                            public MediaType contentType() {
                                return null;
                            }

                            @Override
                            public long contentLength() {
                                // Means we don't know the length beforehand.
                                return -1;
                            }

                            @Override
                            public BufferedSource source() {
                                try {
                                    return new Buffer().readFrom(stream);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    return null;
                                }
                            }
                        })
                        .build();
            }
        });
        return client;
    }

}
